package fr.utt.lo02.projet8americain.model;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import fr.utt.lo02.projet8americain.model.player.PhysicalPlayer;
import fr.utt.lo02.projet8americain.model.player.Player;
import fr.utt.lo02.projet8americain.model.player.VirtualPlayer;
import fr.utt.lo02.projet8americain.model.settings.Setting;
import fr.utt.lo02.projet8americain.model.settings.Settings;

/**
 * Singleton gerant les differents fonctions de la partie (jouer
 * une carte, piocher, joueur suivant, autoriser les joueurs e
 * jouer...).
 * 
 * @author Clement
 *
 */
public class Game extends Observable {	
	private static Game instance;

	protected Player[] players;
	protected ArrayDeque<Card> discardPile;
	protected PhysicalPlayer realPlayer;

	protected RandomizedArrayList stockPile;

	protected int nextPlayer = 1, currentPlayer = 0;
	private Direction direction = Direction.RIGHT;

	private int turn = 0, round = 0;

	private ArrayList<GameActionListener> listeners = new ArrayList<GameActionListener>();

	protected Game() {
		// Initialisation du joueur reel
		realPlayer = new PhysicalPlayer("Joueur 1");

		// Creation de la pioche et de la defausse
		stockPile = new RandomizedArrayList();
		discardPile = new ArrayDeque<Card>();
	}

	/**
	 * Fonction appelee par un joueur pour piocher une carte
	 * @param player joueur voulant piocher
	 */
	public synchronized void pickCard(Player player) {
		if (applyModifiers(player, null)) {
			// On lui donne une carte qu'on recupere
			this.give(player, 1);

			// Si on doit piocher jusqu'a ce qu'on puisse jouer
			if (Setting.PICK_UNTIL_PLAY.getBoolean()) {
				// On reautorise le joueur a jouer
				player.setCanPlay(true, player.isTurn());
			} else {
				// Tour suivant
				nextTurn();
			}

		}
	}

	/**
	 * Fonction appelee par un joueur pour poser une carte
	 * @param source joueur posant la carte
	 * @param card carte posee
	 */
	public synchronized void cardPlayed(Player source, Card card) {
		// Si l'arret n'est pas force (mauvaise carte jouee, pas le tour du joueur...)
		// et que la carte peut bien etre jouee (si une penalite n'est aps deja tombee
		if (applyModifiers(source, card) && Settings.getVariant().canPlayOver(card, this.discardPile.element())) {
			// On pose la carte sur le talon
			this.discardPile.push(card);
			
			// On applique les effets
			Settings.getVariant().onCardPlayed(source, card, this);

			// On notifie que la carte a ete jouee
			for (Iterator<GameActionListener> iter = listeners.iterator(); iter.hasNext();) {
				iter.next().onCardPlayed(source, card);
			}

			nextTurn();
		} else {

			// On rends la carte au joueur
			source.getHand().add(card);

			// On reautorise les meme joueurs a jouer
			Settings.getUnlocker().unlock(this, players);
		}
	}

	/**
	 * Applique les modidicateurs de jeu. Si aucun probleme n'est trouve le tour
	 * est termine et la carte posee.
	 * Sinon le joueur actuel recupere sa carte et doit jouer
	 * 
	 * @param source joueural'origine de l'action
	 * @param card carte jouee
	 * @return vrai si le tour est valide
	 */
	private boolean applyModifiers(Player source, Card card) {
		boolean cancelTurn = false;

		// On appelle tous les modificateurs avant le jeu (arret si stop = true)
		for (Iterator<PlayHandlingModifier> iter = Settings.getPlayModifiers().iterator(); iter.hasNext() && !cancelTurn;) {
			// Si l'application renvoie true la boucle va se terminer
			cancelTurn = iter.next().onCardPlayed(this, source, card);
		}

		// Tour valide/non annule, les joueurs ne peuvent plus jouer
		if (!cancelTurn) {
			cancelPlayers();
		}

		return !cancelTurn;
	}

	/**
	 * Enleve aux joueurs le droit de jouer
	 */
	public void cancelPlayers() {
		for (Player p : players) {
			p.setCanPlay(false, false);
		}
	}

	/**
	 * Lance le tour suivant
	 */
	public void nextTurn() {
		// On compte les points du tour pour le joueur courant
		boolean over = Settings.getCountingMethod().countTurn(players, getCurrentPlayer());

		Settings.nextTurn();

		// Manche terminee
		if (over) {

			// Si la partie est terminee
			if (Settings.getCountingMethod().countRound(players)) {
				// On declare la partie comme terminee
				Settings.setStarted(false);

				// On notifie l'interface
				this.setChanged();
				this.notifyObservers();

			} else {
				// Sinon on commence le prochain round
				nextRound();
			}

			return;
		}

		// On ajoute 1 au tour
		this.turn ++;

		// On notifie l'interface
		this.setChanged();
		this.notifyObservers();

		// On modifie le joueur courant et le joueur suivant (selon le sens)
		this.currentPlayer = this.nextPlayer;
		this.setNextPlayer(this.currentPlayer + direction.getShift(), direction);

		// On lance le tour suivant
		Settings.getUnlocker().unlock(this, players);
	}

	/**
	 * Demarre une manche
	 */
	public void nextRound() {
		this.turn = 0;
		this.round ++;

		// On notifie l'interface de la nouvelle manche
		this.setChanged();
		this.notifyObservers();

		// On distribue les cartes
		Settings.getCardDealer().deal(stockPile, discardPile, players);

		// On met le nombre de carte suffisant dans la defausse
		for (int i = 0; i < Setting.DISCARD_MIN_STORAGE.getValue(); i++) {
			discardPile.add(stockPile.pick());
		}

		nextTurn();
	}

	/**
	 * Donne le nombre indique de cartes de la pioche au joueur donne
	 * @param player joueur recevant les cartes
	 * @param cards nombre de cartes a donner
	 * @return cartes piochees par le joueur
	 */
	public Card[] give(Player player, int cards) {
		Card[] picked;

		// Si il n'y a pas asser de cartes
		if (stockPile.size() < cards) {
			// On vide la defausse
			stockPile.restore(discardPile);
		}

		// S'il n'y a toujours pas suffisement de cartes, on donne tout
		if (stockPile.size() < cards) {
			cards = stockPile.size();
		}

		// On garde le nombre de cartes piochees pour les renvoyer
		picked = new Card[cards];

		// On prend le nombre de cartes necessaires
		synchronized(player.getHand()) { // modifications concourantes
			for (int i = 0; i < cards; i++) {
				picked[i] = stockPile.pick();
	
				// On ajoute les cartes retirees
				player.getHand().add(picked[i]);
			}
		}
		// Notification des cartes piochees
		for (Iterator<GameActionListener> iter = listeners.iterator(); iter.hasNext();) {
			iter.next().onCardsPicked(player, picked);
		}

		return picked;
	}

	/**
	 * Renvoie le joueur qui est suppose jouerace tour
	 * @return joueur actuel
	 */
	public Player getCurrentPlayer() {
		return players[currentPlayer];
	}

	/**
	 * Demarre une partie
	 */
	public synchronized void start() {
		// Si la partie est deja commencee ou qu'on ne peut pas la commencer
		if (Settings.isStarted() || !Settings.canStart()) {
			System.err.println("Some settings are not defined or the game has already begun");
			return;
		}

		// Sinon demarre la partie
		// Creation des joueurs
		players = new Player[Setting.PLAYERS_NUMBER.getValue()];
		players[0] = realPlayer;

		for (int i = 1; i < players.length; i++) {
			players[i] = new VirtualPlayer(Settings.getStrategy());
		}

		// On initialise la methode de comptage et les differentes interfaces/effets
		Settings.init(this);

		// On genere le paquet de cartes
		stockPile.generate(Setting.DECKS_NUMBER.getValue());

		// On notifie les interface du debut du jeu
		Settings.setStarted(true);
		
		// Debut du premier round
		this.nextRound();

	}

	/**
	 * Defini le joueur suivant
	 * @param index indice du joueur
	 * @param direction sens de parcours si le joueur a termine de jouer
	 */
	public void setNextPlayer(int index, Direction direction) {
		int tries = 0;

		index = (index + players.length) % players.length;

		// Tant que le joueur donneala main vide
		while (players[index].getHand().size() == 0 && tries < players.length + 1) {
			// On passe au joueur suivant
			index = direction.apply(index + players.length) % players.length;
			tries ++;
		}

		this.nextPlayer = index;
	}

	/**
	 * @return l'indice du joueur actuel
	 */
	public int getCurrentPlayerIndex() {
		return this.currentPlayer;
	}

	/**
	 * 
	 * @return 1 si l'ordre de jeu est normal et -1 s'il est inverse
	 */
	public Direction getDirection() {
		return direction;
	}

	public void reverseDirection() {
		this.direction = direction.getOpposite();
	}


	

	/**
	 * Renvoie l'indice d'un joueur
	 * @param player joueur
	 * @return indice de ce joueur dans le tableau
	 */
	public int getPlayerIndex(Player player) {
		for (int i = 0; i < players.length; i++) {
			if (players[i] == player) {
				return i;
			}
		}

		return -1;
	}
	
	
	/*
	 * =======================================================================
	 * 		Getters / setter
	 * =======================================================================
	 */

	/**
	 * Renvoie le joueur humain de la partie
	 * @return Joueur physique
	 */
	public PhysicalPlayer getPhysicalPlayer() {
		return realPlayer;
	}

	public RandomizedArrayList getStockPile() {
		return stockPile;
	}

	public ArrayDeque<Card> getDiscardPile() {
		return discardPile;
	}

	public Player getNextPlayer() {
		return players[nextPlayer];
	}

	public Player[] getPlayers() {
		return players;
	}

	public int getNextPlayerIndex() {
		return nextPlayer;
	}
	
	public int getTurn() {
		return turn;
	}

	public int getRound() {
		return round;
	}

	public void addActionListener(GameActionListener listener) {
		this.listeners.add(listener);
	}

	/**
	 * Renvoie l'instance de la partie actuelle, en cree une si elle n'existe pas
	 * @return instance de partie
	 */
	public static Game getInstance() {
		if (instance == null) {
			instance = new Game();
		}

		return instance;    	
	}
}

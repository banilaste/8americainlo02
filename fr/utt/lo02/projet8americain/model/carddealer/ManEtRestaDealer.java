package fr.utt.lo02.projet8americain.model.carddealer;

import fr.utt.lo02.projet8americain.model.RandomizedArrayList;
import fr.utt.lo02.projet8americain.model.player.Player;
import fr.utt.lo02.projet8americain.model.settings.Setting;

/**
 * Singleton gerant une distribution des cartes avec un nombre de cartes choisi par l'utilisateur
 * @author Clement
 *
 */
public class ManEtRestaDealer extends CardDealer {
	private static ManEtRestaDealer instance;
	
	// Singleton
	private ManEtRestaDealer() {}
	
	@Override
	protected int cardDistributed(RandomizedArrayList stockPile, Player[] players) {
		// Nombre de cartes distribuees par joueur
		int cards = Setting.CARD_NUMBER.getValue();

		// On verifie qu'il y a suffisemment de cartes sinon on met le max en dessous
		if (stockPile.size() < players.length / cards) {
			cards = (int) Math.floor(stockPile.size() / players.length);
		}

		return cards;
	}

	public String toString() {
		return "man et resta (" + Setting.CARD_NUMBER.getValue() + " cartes)";
	}
	
	/**
	 * Renvoie l'instance
	 * @return instance unique
	 */
	public static ManEtRestaDealer get() {
		if (instance == null) {
			instance = new ManEtRestaDealer();
		}
		
		return instance;
	}
}

package fr.utt.lo02.projet8americain.model.carddealer;

import java.util.ArrayDeque;
import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.RandomizedArrayList;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Classe abstraite gerant la distribution.
 * 
 * Ses implementations creent la fonction <b>cardDistributed</b> qui renvoie le nombre de
 * cartes devant etre distribue.
 * 
 * @author Clement
 *
 */
public abstract class CardDealer {
	/**
	 * Distribue les cartes. Les cartes sont supposees reparties entre les joueurs et
	 * la pile.
	 * @param stockPile pioche
	 * @param discardPile defausse
	 * @param players joueurs de la partie
	 */
	public final void deal(RandomizedArrayList stockPile, ArrayDeque<Card> discardPile, Player[] players) {
		int cards;

		// On commence mettre la defausse dans la pioche
		stockPile.addAll(discardPile);

		// Puis la vider
		discardPile.clear();

		// Recuperer les mains des joueurs
		for (int i = 0; i < players.length; i++) {
			// Ajout des cartes puis vidage des mains
			stockPile.addAll(players[i].getHand());
			players[i].getHand().clear();
		}

		// Puis calcul du nombre de cartes distribuees
		cards = this.cardDistributed(stockPile, players);

		// Pour chaque joueur
		for (int i = 0; i < players.length; i++) {

			// On distribue N cartes prises aleatoirement
			for (int j = 0; j < cards; j++) {
				players[i].getHand().add(stockPile.pick());
			}
		}
	}

	/**
	 * Renvoie le nombre de cartes distribuees
	 * @param stockPile pioche
	 * @param players joueurs de la partie
	 * @return nombre de cartes distribuees
	 */
	protected abstract int cardDistributed(RandomizedArrayList stockPile, Player[] players);
}

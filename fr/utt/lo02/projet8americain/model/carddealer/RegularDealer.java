package fr.utt.lo02.projet8americain.model.carddealer;

import fr.utt.lo02.projet8americain.model.RandomizedArrayList;
import fr.utt.lo02.projet8americain.model.player.Player;
import fr.utt.lo02.projet8americain.model.settings.Setting;

/**
 * Distribution classique des cartes. Le nombre de cartes distribue est determine
 * selon une fonction du nombre de joueur et du nombre de decks
 * @author Clement
 *
 */
public class RegularDealer extends CardDealer {
	private static RegularDealer instance;

	private RegularDealer() {}

	@Override
	protected int cardDistributed(RandomizedArrayList stockPile, Player[] players) {
		// Nombre de paquets
		int decks = Setting.DECKS_NUMBER.getValue();

		// 2 decks -> 1.5x plus de cartes, 10 cartes pour 2 joueur puis 2
		// en moins tous les joueurs jusqu'a 4
		int cards = (int) (Math.pow(1.5, decks - 1) * (14 - 2 * Math.min(4, players.length)));
		
		return cards;
	}
	
	public String toString() {
		return "normale";
	}
	
	/**
	 * Renvoie l'instance
	 * @return instance
	 */
	public static RegularDealer get() {
		if (instance == null) {
			instance = new RegularDealer();
		}
		
		return instance;
	}
}

package fr.utt.lo02.projet8americain.model;

/**
 * Classe representant une carte
 * @author Clement
 *
 */
public class Card {
    private Color color;
    private Value value;
    
    public Card(Color color, Value value) throws InvalidCardException {
		this.color = color;
		this.value = value;
		
		// On n'autorise pas les valeurs nulles
		if (color == null || value == null) {
			throw new InvalidCardException();
		}
	}

    /**
     * Renvoie la couleur de la carte
     * @return couleur de la carte
     */
	public Color getColor() {
		return color;
	}

	/**
	 * Renvoie la valeur de la carte
	 * @return valeur de la carte
	 */
	public Value getValue() {
		return value;
	}

	/**
	 * Recupere une carte selon sa description. Renvoie null pour une description invalide
	 * @param descriptor description textuelle de la carte
	 * @return carte associee
	 * @throws InvalidCardException erreur lorsque la carte decrite n'existe pas
	 */
	public static Card parseCard(String descriptor) throws InvalidCardException {
		String[] parts;

		// Pas de description -> pas de carte
		if (descriptor == null) {
			throw new InvalidCardException();
		}
		
		parts = descriptor.split(" ");
		
		// Si moins de 2 parties -> pas de carte
		if (parts.length < 2) {
			throw new InvalidCardException();
		}
		
		Value value = Value.fromString(parts[0]);
		Color color = Color.fromString(parts[1]);
		
		// Si l'un des deux est invalide
		if (value == null || color == null) {
			throw new InvalidCardException("valeur ou couleur incorrecte");
		}
		
		return new Card(color, value);
	}
	
    public boolean equals(Object c) {
		if (c instanceof Card) {
			Card card = (Card) c;
			
			return (card.getColor() == color || color == null || card.getColor() == null)
					&& card.getValue() == value;
		} else {
			return c == this;
		}
    }
    
    public String toString() {
		return value + " " + color;
    }

    
}

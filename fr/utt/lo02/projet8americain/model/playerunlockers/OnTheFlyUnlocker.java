package fr.utt.lo02.projet8americain.model.playerunlockers;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.LinkedList;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.PlayHandlingModifier;
import fr.utt.lo02.projet8americain.model.Value;
import fr.utt.lo02.projet8americain.model.player.Player;
import fr.utt.lo02.projet8americain.model.settings.Setting;
import fr.utt.lo02.projet8americain.model.settings.Settings;

public class OnTheFlyUnlocker extends PlayHandlingModifier implements PlayerUnlocker {
	private static OnTheFlyUnlocker instance;

	private PlayerUnlocker unlocker;
	
	/**
	 * Joueurs autorisesajouer par dessus
	 */
	private LinkedList<Player> authorized = new LinkedList<Player>();
	private Value authorizedValue;
	
	private OnTheFlyUnlocker (PlayerUnlocker unlocker) {
		this.unlocker = unlocker;
	}
	
	
	public void init(Game game) {
		unlocker.init(game);
		
		// On a besoin de 3 cartes minimum dans la defausse
		Settings.set(Setting.DISCARD_MIN_STORAGE, 3);
	}
	
	public void unlock(Game game, Player[] players) {
		// On commence par debloquer tous les joueurs avec le debloqueur de priorite inferieure
		unlocker.unlock(game, players);
		
		// Puis on cherche les joueurs pouvant jouerala volee
		int count = 0;
		
		// On recupere la defausse
		ArrayDeque<Card> discard = game.getDiscardPile();
		
		// La valeuracomparer	
		authorizedValue = discard.element().getValue();
		
		// Si les trois dernieres cartes ont la meme valeur
		for (Iterator<Card> iter = discard.iterator(); iter.hasNext();) {
			// On compare seulement la valeur
			if (iter.next().getValue() == authorizedValue) {
				count ++;
			} else {
				break;
			}
		}
		
		// Si trois carte de meme valeur ont ete jouees
		if (count == 3) {
			// On cherche les joueurs possedant cette carte
			for (int i = 0; i < players.length; i++) {
				LinkedList<Card> hand = players[i].getHand();
				
				for (Iterator<Card> iter = hand.iterator(); iter.hasNext();) {
					if (iter.next().getValue() == authorizedValue) {
						// On l'ajoute aux joueurs autorises
						authorized.add(players[i]);
						
						// Si il ne peut pas jouer, on l'y autorise
						if (!players[i].canPlay()) {
							players[i].setCanPlay(true, false);
						}
					}
				}
				
			}
		}
		
	}
	
	public boolean onCardPlayed(Game game, Player source, Card card) {
		// Si le joueur est autoriseajouer, que ce n'est pas son tour, et que 
		// ce n'est pas la bonne carte
		if (authorized.contains(source) && !source.isTurn() && card.getValue() != authorizedValue) {
			// On annule son tour
			return true;
		}
		
		// Si le joueur n'est pas le joueur courant
		if (source != game.getCurrentPlayer()) {
			// On le definit comme le joueur suivant
			game.setNextPlayer(game.getPlayerIndex(source), game.getDirection());
		}
		
		// Sinon on se refere au debloqueur specifie si besoin est
		if (unlocker instanceof PlayHandlingModifier) {
			return ((PlayHandlingModifier) unlocker).onCardPlayed(game, source, card);
		}
		
		// Sinon pas de raisons de s'opposer au tour
		return false;
	}
	
	public static OnTheFlyUnlocker getInstance() {
		if (instance == null) {
			instance = new OnTheFlyUnlocker(NextPlayerUnlocker.getInstance());
		}
		
		return instance;
	}
}

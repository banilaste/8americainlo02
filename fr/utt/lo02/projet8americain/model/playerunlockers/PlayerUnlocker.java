package fr.utt.lo02.projet8americain.model.playerunlockers;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Interface permettant d'autoriser certains joueursajouer ou pas.
 * @author Clement
 *
 */
public interface PlayerUnlocker {
	/**
	 * Initialise les parametres
	 * @param game
	 */
	public void init(Game game);
	
	/**
	 * Autorise les joueursajouer
	 * @param game Partie courante
	 * @param players Joueurs de la partie
	 */
	public void unlock(Game game, Player[] players);
}

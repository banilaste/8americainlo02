package fr.utt.lo02.projet8americain.model.playerunlockers;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.PlayHandlingModifier;
import fr.utt.lo02.projet8americain.model.player.Player;
import fr.utt.lo02.projet8americain.model.settings.Setting;
import fr.utt.lo02.projet8americain.model.settings.Settings;

/**
 * Modificateur de jeu permettant de faire des erreurs qui seront penalisees.
 * Jouer hors de son tour ou la mauvaise carte resultera en une penalite.
 * @author Clement
 *
 */
public class AllowErrorsUnlocker extends PlayHandlingModifier implements PlayerUnlocker  {
	private static AllowErrorsUnlocker instance;
	
	private AllowErrorsUnlocker() {}
	
	public void init(Game game) {
		// Les bots doivent attendre dans notre cas
		Settings.set(Setting.VIRTUAL_PLAYER_DELAY, true);
		
		// On s'enregistre dans les modificateurs de jeu
		Settings.getPlayModifiers().add(this);
	}
	
    public void unlock(Game game, Player[] players) {
    	Player next = game.getCurrentPlayer();

    	// On autorise tous les joueursajouer
		for (int i = 0; i< players.length; i++) {
    		players[i].setCanPlay(true, next == players[i]);
    	}
    }
    
	public boolean onCardPlayed(Game game, Player source, Card card) {
		// On verifie que le joueur est bien autoriseajouer (c'est son tour)
		if (!source.isTurn()) {
			// Si ce n'est pas le cas, on inflige une penalite et on annule le tour
			penalize(source, game);
			return true;
		}
		
		// On verifie bien que la carte peut etre jouee
		if (card != null && !Settings.getVariant().canPlayOver(card, game.getDiscardPile().element())) {
			// Si ce n'est pas le cas, on inflige une penalite et on annule le tour
			penalize(source, game);
			return true;
		}
		
		// Le tour n'est pas annule
		return false;
	}
	
	/**
	 * Donne une penalite au joueur selon les parametres definis
	 * @param player Joueur penalise
	 * @param game Partie courante
	 */
	private void penalize(Player player, Game game) {
		game.give(player, Setting.ERROR_PENALITY.getValue());
	}
	
	public static AllowErrorsUnlocker getInstance() {
		if (instance == null) {
			instance = new AllowErrorsUnlocker();
		}
		
		return instance;
	}
}

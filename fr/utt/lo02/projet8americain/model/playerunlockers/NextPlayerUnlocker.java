package fr.utt.lo02.projet8americain.model.playerunlockers;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Classe permettant au joueur suivant de jouer
 * @author Clement
 *
 */
public class NextPlayerUnlocker implements PlayerUnlocker {
	private static NextPlayerUnlocker instance;
	
	private NextPlayerUnlocker() {}
	
	public void init(Game game) {}
	
    public void unlock(Game game, Player[] players) {
    	// On debloque seulement le joueur actuel
    	game.getCurrentPlayer().setCanPlay(true, true);
    }
    
    public static NextPlayerUnlocker getInstance() {
    	if (instance == null) {
    		instance = new NextPlayerUnlocker();
    	}
    	
		return instance;
    }
}

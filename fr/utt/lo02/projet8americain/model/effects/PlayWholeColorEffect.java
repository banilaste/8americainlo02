package fr.utt.lo02.projet8americain.model.effects;

import java.util.Iterator;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Color;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Joue toutes les cartes de meme couleur d'un
 * joueur (les place dans la pioche directement).
 * 
 * @author Clement
 *
 */
public class PlayWholeColorEffect extends Effect {

	public PlayWholeColorEffect(String[] args) {
		super(args);
	}
	
	public void trigger(Game game, Card card, Player source) {
		Color color = card.getColor();
		Card next;
		
		Iterator<Card> iter = source.getHand().iterator();
		
		while(iter.hasNext()) {
			next = iter.next();
			
			// Si meme couleur
			if (next.getColor() == color) {
				// On la pose sur le tas
				game.getDiscardPile().add(next);
				
				// On supprime de la main
				iter.remove();
			}
		}
	}

	@Override
	public int getPriority() {
		// On veut jouer les cartes avant les effets donnant des cartes
		return HIGH * 2;
	}

	@Override
	public String toString() {
		return "pose toute les cartes de meme couleur";
	}
}

package fr.utt.lo02.projet8americain.model.effects;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Permet au joueur de rejouer un tour
 * @author Clement
 *
 */
public class ReplayEffect extends Effect { 

	public ReplayEffect(String[] args) {
		super(args);
	}

	@Override
	public void trigger(Game game, Card card, Player source) {
		game.setNextPlayer(game.getCurrentPlayerIndex(), game.getDirection());
	}

	@Override
	public int getPriority() {
		// On ne veut pas recevoir des penalites en etant soi-meme le prochain joueur
		return LOW;
	}

	@Override
	public String toString() {
		return "permet de rejouer";
	}

}

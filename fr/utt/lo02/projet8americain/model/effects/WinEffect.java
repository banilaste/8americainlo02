package fr.utt.lo02.projet8americain.model.effects;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Effet permettantaun joueur de gagner
 * @author Clement
 *
 */
public class WinEffect extends Effect {

	public WinEffect(String[] args) {
		super(args);
	}

	@Override
	public void trigger(Game game, Card card, Player source) {
		// On ajoute toutes les cartes du joueurs dans la pioche
		game.getStockPile().addAll(source.getHand());
		
		// Et on vide la main
		source.getHand().clear();
	}

	@Override
	public int getPriority() {
		// On fait gagner le joueur peu importe les effets avant
		return LOW;
	}
	
	public String toString() {
		return "gagne la manche";
	}
}

package fr.utt.lo02.projet8americain.model.effects;

/**
 * Exception levee lorsqu'un recoit des arguments incorrects
 * @author Clement
 *
 */
public class InvalidEffectArguments extends Exception {
	private static final long serialVersionUID = 1L;
	
	// Messages generiques pour des erreurs classiques
	public static final String MISSING_ARGUMENTS = "missing $ arguments";
	public static final String INVALID_VALUE = "$ is not a valid value";
	public static final String INVALID_COLOR = "$ is not a valid color";
	public static final String INVALID_ENUM_VALUE = "$ is not a valid value (available values: $)";
	public static final String INVALID_EFFECT = "$ is not a valid effect name";
	public static final String INVALID_NUMBER = "$ is not a valid number";

	public InvalidEffectArguments(String string) {
		super(string);
	}
	
	/**
	 * Cree une erreur avec un message type specifie et un argument
	 * @param arg argument
	 * @param type message type
	 */
	public InvalidEffectArguments(Object arg, String type) {
		super(getMessage(type, new Object[] {arg}));
	}
	
	/**
	 * Cree une erreur avec un message type specifie et plusieurs
	 * @param args arguments
	 * @param type message type
	 */
	public InvalidEffectArguments(Object args[], String type) {
		super(getMessage(type, args));
	}
	
	/**
	 * Renvoie le message type complete par des arguments
	 * @param description message contenant $ devant etre remplacees par des arguments
	 * @param arguments argumentsaplacer
	 * @return chaine constuite
	 */
	private static String getMessage(String description, Object arguments[]) {		
		for (Object arg : arguments) {
			// Si c'est un tableau, on construit une liste separee par des ,
			if (arg.getClass().isArray()) {
				StringBuffer sb = new StringBuffer();
				Object objects[] = (Object[]) arg;
				
				for (int i = 0; i < objects.length; i++) {
					sb.append(objects[i].toString());
					sb.append(", ");
				}
				
				// on supprime le dernier espace si il y en a eu
				if (objects.length > 0) {
					sb.delete(sb.length() - 2, sb.length());
				}
				
				description = description.replaceFirst("\\$", sb.toString());
			} else {
				description = description.replaceFirst("\\$", arg.toString());
			}
		}
		
		return description;
	}
}

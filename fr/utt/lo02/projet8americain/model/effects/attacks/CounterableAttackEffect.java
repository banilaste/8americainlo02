package fr.utt.lo02.projet8americain.model.effects.attacks;

import java.util.Iterator;
import java.util.LinkedList;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.effects.Effect;
import fr.utt.lo02.projet8americain.model.effects.InvalidEffectArguments;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Effet donnant le nombre de cartes specifie en entree au joueur
 * suivant avec possibilite de contrer
 * 
 * Arguments pris : nombre de carte donne, effet en cas de relance de carte
 * @author Clement
 *
 */
public class CounterableAttackEffect extends Effect {
	private static int cards, initialAmount;
	private static Player target;
	
	private int attack;
	private CounterAttackEffect innerEffect;
	
	public CounterableAttackEffect(String[] args) throws InvalidEffectArguments {
		super(args);
		
		try {
			attack = Integer.parseInt(args[0]);
			
			innerEffect = new CounterAttackEffect(new String[] {args[1]});
		} catch(NumberFormatException e) {
			throw new InvalidEffectArguments(args[0], InvalidEffectArguments.INVALID_NUMBER);
			
		} catch (IndexOutOfBoundsException e) {
			throw new InvalidEffectArguments(1, InvalidEffectArguments.MISSING_ARGUMENTS);
		}
	}

	@Override
	public void trigger(Game game, Card card, Player source) {
		// On contre une attaque actuelle si besoin
		innerEffect.trigger(game, card, source);
		
		// On verifie si le joueur suivant peut contrer
		LinkedList<Card> hand = game.getNextPlayer().getHand();
		
		for (Iterator<Card> iter = hand.iterator(); iter.hasNext();) {
			// Si une carte peut contrer
			if (CounterAttackEffect.canCounter(iter.next().getValue())) {
				// Si aucune attaque n'est en cours
				if (cards == 0) {
					// On en initialise une
					cards = attack;
					initialAmount = cards;

					setTarget(game.getNextPlayer());
				}
				// et on en termine la
				return;
			}
		}
		
		// Sinon on lance l'attaque contre lui
		if (cards == 0) {
			// Si rien n'est fait, on lance
			cards = attack;
		}
		
		setTarget(game.getNextPlayer());
		execute();
		
	}
	
	/**
	 * Lance l'attaque
	 */
	public static void execute() {
		Game.getInstance().give(target, cards);
		
		stop();
	}

	/**
	 * Ajoute le montant initial de cartesal'attaque
	 */
	public static void add() {
		add(initialAmount);
	}
	
	/**
	 * Ajoute le nombre de carte indiqueal'attaque
	 * @param amount
	 */
	public static void add(int amount) {
		cards += amount;
	}

	/**
	 * Arrete l'attaque
	 */
	public static void stop() {
		cards = 0;
		initialAmount = 0;
	}

	/**
	 * Renvoie la cible de l'attaque
	 * @return cible de l'attaque
	 */
	public static Player getTarget() {
		return target;
	}

	public static void setTarget(Player target) {
		CounterableAttackEffect.target = target;
	}

	@Override
	public int getPriority() {
		return HIGH;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("le joueur suivant piochera ");
		buffer.append(attack);
		buffer.append(" cartes (contrable), ");
		buffer.append(innerEffect.toString());
		
		return buffer.toString();
	}
}

package fr.utt.lo02.projet8americain.model.effects.attacks;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.effects.Effect;
import fr.utt.lo02.projet8americain.model.effects.InvalidEffectArguments;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Effet donnant le nombre de cartes specifie en entree au joueur suivant sans possibilite de contrer
 * @author Clement
 *
 */
public class AttackEffect extends Effect {
	private int cards = 1;
	
	public AttackEffect(String[] args) throws InvalidEffectArguments {
		super(args);
		
		
		// On a besoin du nombre de carte specifie en argument
		if (args.length == 0) {
			throw new InvalidEffectArguments(1, InvalidEffectArguments.MISSING_ARGUMENTS);
		}

		try {
			cards = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			throw new InvalidEffectArguments(args[0], InvalidEffectArguments.INVALID_NUMBER);
		}
	}
	
	public void trigger(Game game, Card card, Player source) {
		game.give(game.getNextPlayer(), cards);
	}

	@Override
	public int getPriority() {
		return HIGH;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("le joueur suivant pioche ");
		buffer.append(cards);
		buffer.append(" cartes");
		
		return buffer.toString();
	}

}

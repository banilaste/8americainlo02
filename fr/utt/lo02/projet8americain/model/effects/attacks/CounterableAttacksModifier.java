package fr.utt.lo02.projet8americain.model.effects.attacks;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.PlayHandlingModifier;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Modificateur de jeu gerant les attaque contrables avant les effets
 * @author Clement
 *
 */
public class CounterableAttacksModifier extends PlayHandlingModifier {
	private static CounterableAttacksModifier instance;
	
	private CounterableAttacksModifier() {}
	
	@Override
	public boolean onCardPlayed(Game game, Player source, Card card) {
		// Si c'est la cible de l'attaque et que la carte ne contre pas
		if (source == CounterableAttackEffect.getTarget() && !CounterAttackEffect.canCounter(card.getValue())) {
			// ON LANCE L'ATTAQUE
			CounterableAttackEffect.execute();
		}
		
		return false;
	}
	
	public static CounterableAttacksModifier getInstance() {
		if (instance == null) {
			instance = new CounterableAttacksModifier();
		}
		
		return instance;
	}
}

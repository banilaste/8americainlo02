package fr.utt.lo02.projet8americain.model.effects.attacks;

import java.util.ArrayList;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.Value;
import fr.utt.lo02.projet8americain.model.effects.Effect;
import fr.utt.lo02.projet8americain.model.effects.InvalidEffectArguments;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Contre l'attaque d'un joueur, l'annule ou la relance selon les parametres de l'effet.
 * @author Clement
 *
 */
public class CounterAttackEffect extends Effect {
	/**
	 * Objet contenant les valeurs pouvant contrer l'attaque
	 */
	private static ArrayList<Value> counters = new ArrayList<Value>();
	
	private ReiterationEffect effect;
	
    public CounterAttackEffect(String[] args) throws InvalidEffectArguments {
    	super (args);
    	
		try {
			effect = ReiterationEffect.valueOf(args[0]);
		} catch (IllegalArgumentException e) {
			throw new InvalidEffectArguments(new Object[] {args[0], ReiterationEffect.values()}, InvalidEffectArguments.INVALID_ENUM_VALUE);
			
		} catch (IndexOutOfBoundsException e) {
			throw new InvalidEffectArguments(1, InvalidEffectArguments.MISSING_ARGUMENTS);
		}
	}

	@Override
	public void init(Game game, Value appliedOn) {
		// On ajoute le modificateur de jeu
		CounterableAttacksModifier.getInstance().enable();
		
		// Et on declare la valeur
		counters.add(appliedOn);
	}
	
	public void trigger(Game game, Card card, Player source) {
    	if (effect == ReiterationEffect.ADD) {
    		// On reporte l'attaque sur le joueur suivant
    		CounterableAttackEffect.add();
    		CounterableAttackEffect.setTarget(game.getNextPlayer());
    		
    	} else if (effect == ReiterationEffect.STOP) {
    		CounterableAttackEffect.stop();
    		
    	} else { // Sinon on lance l'attaque sur le joueur courant
    		CounterableAttackEffect.execute();
    	}
    }


	@Override
	public boolean canPlayOver(Card card, Card topCard, boolean defaultValue) {
		return defaultValue;
	}


	/**
	 * Renvoie un booleen indiquant si l'attaque peut etre contree avec une valeur
	 * @param value valeur de la carte
	 * @return booleen indiquant si la valeur peut etre contree
	 */
	public static boolean canCounter(Value value) {
		return counters.contains(value);
	}
	
	protected enum ReiterationEffect {
		ADD, // on ajoute le nombre de carte initialal'attaque et on passe au suivant
		STOP, // annule simplement l'attaque
		DISABLED // ne fait rien sur l'attaque, (l'effet est annule)
	}

	@Override
	public int getPriority() {
		return HIGH;
	}

	@Override
	public String toString() {
		if (effect == ReiterationEffect.ADD) {
			return "reporte une attaque contrable sur le joueur suivant"; 
		} else if (effect == ReiterationEffect.STOP) {
			return "contre une attaque contrable";
		}
		
		return "";
	}
}

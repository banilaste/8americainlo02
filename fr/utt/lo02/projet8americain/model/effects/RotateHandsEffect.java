package fr.utt.lo02.projet8americain.model.effects;

import java.util.LinkedList;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Direction;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Donne la main de chaque joueur au joueuragauche ouadroite
 * selon les arguments specifies
 * @author Clement
 *
 */
public class RotateHandsEffect extends Effect {	
	private Direction direction = Direction.LEFT;
	
	public RotateHandsEffect(String[] args) throws InvalidEffectArguments {
		super(args);
		
		try {
			direction = Direction.valueOf(args[0]);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new InvalidEffectArguments(1, InvalidEffectArguments.MISSING_ARGUMENTS);
		} catch (IllegalArgumentException e) {
			throw new InvalidEffectArguments(new Object[] {args[0], Direction.values()}, InvalidEffectArguments.INVALID_ENUM_VALUE);
		}
	}

	@Override
	public void trigger(Game game, Card card, Player source) {
		Player[] players = game.getPlayers();
		
		int index = get(direction, 0, players), lastIndex = -1;

		// Premiere main a etre ecrasee
		LinkedList<Card> savedHand = players[index].getHand();
		Player lastPlayer = players[index];
		
		// On s'arrete au moment ou le joueur suivantadonner son jeu est celui dont la main a ete gardee
		do {
			// Calcul du nouvel indice et sauvegarde du precedent
			lastIndex = index;
			index = next(direction, index, players);
			
			// Affectation du jeu au jouer associe
			players[lastIndex].setHand(players[index].getHand());
		} while(lastPlayer != players[next(direction, index, players)]);
		
		// On donneace moment la le dernier jeu
		players[index].setHand(savedHand);
	}
	
	/**
	 * Renvoie l'indice donne decale selon les joueurs ne pouvant plus jouer
	 * @param direction direction de deplacement
	 * @param index indice initial
	 * @param players joueurs de la partie
	 * @return indice tenant compte des joueurs ayant une main vide
	 */
	private int get(Direction direction, int index, Player[] players) {	
		// Tant que le joueur selectionne est hors jeu
		while (!players[index].isPlaying()) {
			// On augmente l'indice
			index = direction.apply(index, players.length);
		}
		
		return index;
	}
	
	/**
	 * Renvoie l'indice donne decale de 1 et selon les joueurs ne pouvant plus jouer
	 * @param direction direction de deplacement
	 * @param index indice initial
	 * @param players joueurs de la partie
	 * @return indice decale de 1 en tenant compte des joueurs ayant une main vide
	 */
	private int next(Direction direction, int index, Player[] players) {
		return get(direction, direction.apply(index, players.length), players);
	}

	@Override
	public int getPriority() {
		return NORMAL;
	}

	@Override
	public String toString() {
		return "chacun donne sa main au joueura" + (direction == Direction.LEFT ? "gauche" : "droite");
	}
	
	
}

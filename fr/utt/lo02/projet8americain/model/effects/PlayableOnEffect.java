package fr.utt.lo02.projet8americain.model.effects;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Value;

/**
 * Effect autorisant un type de carteajoueur sur un autre type de carte
 * @author Clement
 *
 */
public class PlayableOnEffect extends Effect {
	private Value targetValue;
	
	public PlayableOnEffect(String[] args) throws InvalidEffectArguments {
		super(args);

		try {
			// Valeur ou la carte peut etre posee
			targetValue = Value.valueOf(args[0]);
		} catch (IndexOutOfBoundsException e) {
			throw new InvalidEffectArguments(1, InvalidEffectArguments.MISSING_ARGUMENTS);
			
		} catch (IllegalArgumentException e) {
			throw new InvalidEffectArguments(args[0], InvalidEffectArguments.INVALID_VALUE);
		}
	}

	@Override
	public boolean canPlayOver(Card card, Card topCard, boolean defaultValue) {
		// Si la carte sur le tas correspondala valeur de notre carte
		if (topCard.getValue() == targetValue) {
			// On peut jouer dessus
			return true;
		}
		
		return defaultValue;
	}

	@Override
	public int getPriority() {
		return NORMAL;
	}

	@Override
	public String toString() {
		return "peut etre jouee sur un " + targetValue;
	}
	
}

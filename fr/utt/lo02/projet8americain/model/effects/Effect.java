package fr.utt.lo02.projet8americain.model.effects;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.Value;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Classe representant un effet applique lorsqu'une carte est jouee
 * @author Clement
 *
 */
public abstract class Effect  {
	/**
	 * Valeur de priorite pour des effet ayant une priorite importante,
	 * c'estadire qui doivent etre lance avant les effets modifiant le
	 * sens de jeu ou le joueur suivant
	 */
	public final static int HIGH = 100;
	
	/**
	 * Valeur de priorite normale, peut etre utilisee pour des effets globaux
	 * ou des effets devant etre lance entre les hautes priorites et les basses
	 */
	public final static int NORMAL = 0;
	
	/**
	 * Valeur de priorite basse, reservee aux effets modifiant le sens du jeu
	 * ou le joueur suivant.
	 */
	public final static int LOW = -100;
	
	/**
	 * Constructeur generique d'un effet
	 * @param args parametres de l'effet
	 */
	public Effect(String[] args) {}

	/**
	 * Initialise l'effet en debut de partie
	 * @param game partie courante
	 */
	public void init(Game game, Value appliedOn) {}
	
	/**
	 * Declenche l'effet
	 * @param game partie courante
	 * @param card carte jouee
	 * @param source joueur ayant pose la carte
	 */
	public void trigger(Game game, Card card, Player source) {}
	
	/**
	 * Selon l'effet indique si un carte peut etre jouee sur une autre.
	 * @param card carte jouee
	 * @param topCard derniere carte jouee
	 * @param defaultValue valeur par defaut
	 * @return booleen indiquant si la carte peut etre jouee selon l'effet
	 */
	public boolean canPlayOver(Card card, Card topCard, boolean defaultValue) {
		return defaultValue;
	}
	
	/**
	 * Renvoie la priorite sous forme d'entier.
	 * Un entier grand signifie une priorite importante.
	 * 
	 * La priorite permet d'eviter des problemes d'effets qui, enchaines de faeon differente,
	 * ne donnent pas le meme resultat (exemple : passer le tour et donner deux cartes au
	 * joueur suivant).
	 * 
	 * @return entier representant la priorite de l'effet
	 */
	public abstract int getPriority();
	
	public abstract String toString();
}

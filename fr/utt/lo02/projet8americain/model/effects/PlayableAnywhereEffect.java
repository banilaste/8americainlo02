package fr.utt.lo02.projet8americain.model.effects;

import fr.utt.lo02.projet8americain.model.Card;

/**
 * Carte pouvant etre jouee sur n'importe quelle carte
 * @author Clement
 *
 */
public class PlayableAnywhereEffect extends Effect {

	public PlayableAnywhereEffect(String[] args) {
		super(args);
	}

	@Override
	public boolean canPlayOver(Card card, Card topCard, boolean defaultValue) {
		// Cette carte peut etre jouee n'importe oe
		return true;
	}

	@Override
	public int getPriority() {
		return NORMAL;
	}

	@Override
	public String toString() {
		return "peut etre jouee n'importe ou";
	}
}

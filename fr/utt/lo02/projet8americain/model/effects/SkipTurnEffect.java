package fr.utt.lo02.projet8americain.model.effects;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Passe le tour du joueur suivant
 * @author Clement
 *
 */
public class SkipTurnEffect extends Effect {

	public SkipTurnEffect(String[] args) {
		super(args);
	}
	
	@Override
	public void trigger(Game game, Card card, Player source) {
		// Joueur suivant
		game.setNextPlayer(game.getDirection().apply(game.getCurrentPlayerIndex()), game.getDirection());
		
		// Joueur d'apres
		game.setNextPlayer(game.getDirection().apply(game.getNextPlayerIndex()), game.getDirection());
	}

	@Override
	public int getPriority() {
		return LOW;
	}

	@Override
	public String toString() {
		return "passe le tour du joueur suivant";
	}

}

package fr.utt.lo02.projet8americain.model.effects;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.player.Askable;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Donne le nombre de cartes specifie au joueur specifie (suivant par defaut)
 * @author Clement
 *
 */
public class GiveCardEffect extends Effect {	
	private int number;
	private Target target;
	private Selection selection;
	
	public GiveCardEffect(String[] args) throws InvalidEffectArguments {
		super(args);
		
		if (args.length < 3) {
			throw new InvalidEffectArguments(3 - args.length, InvalidEffectArguments.MISSING_ARGUMENTS);
		}
		
		try {
			// Nombre de cartes donnees
			number = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			throw new InvalidEffectArguments(args[0], InvalidEffectArguments.INVALID_VALUE);
		}
		
		// Cible et methode de selection des cartes
		try {
			target = Target.valueOf(args[1]);
		} catch (IllegalArgumentException e) {
			throw new InvalidEffectArguments(new Object[] {args[1], Target.values()}, InvalidEffectArguments.INVALID_ENUM_VALUE);
		}
		
		try {
			selection = Selection.valueOf(args[2]);
		} catch (IllegalArgumentException e) {
			throw new InvalidEffectArguments(new Object[] {args[2], Selection.values()}, InvalidEffectArguments.INVALID_ENUM_VALUE);
		}
	}

	@Override
	public void trigger(Game game, Card card, Player source) {
		Player targetPlayer = game.getNextPlayer();
		Card cards[], givenCard;
		
		// Joueur suivant
		if (target == Target.NEXT) {
			targetPlayer = game.getNextPlayer();
		}
		
		// Joueur choisi par le joueur
		else {
			targetPlayer = (Player) source.ask(Askable.PLAYER, game.getPlayers(), game.getNextPlayer());
		}
		
		// On donne X carte dans la limite de la taille de la main du joueur
		for (int i = 0; i < number && source.getHand().size() > 0; i++) {
			
			// Choix de la carte par le joueur
			if (selection == Selection.CHOOSE) {
				cards = new Card[source.getHand().size()];
				cards = source.getHand().toArray(cards);
				
				// On demande quelle carte
				givenCard = (Card) source.ask(Askable.CARD, cards, source.getHand().element());
				
				// On l'enleve du jeu pour la donneral'autre
				source.getHand().remove(givenCard);
				targetPlayer.getHand().add(givenCard);
			}
			
			// Carte aleatoire
			else {
				targetPlayer.getHand()
					.add(source.getHand().remove((int)(Math.random() * source.getHand().size())));
			}
			
		}
		
	}
	
	private enum Target {
		NEXT, CHOOSE
	}
	
	private enum Selection {
		RANDOM, CHOOSE
	}

	@Override
	public int getPriority() {
		if (target == Target.NEXT) {
			return HIGH;
		} else {
			return NORMAL;
		}
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer("donne une carte ");
		
		if (selection == Selection.RANDOM) {
			buf.append("aleatoire");
		} else if (selection == Selection.CHOOSE) {
			buf.append("choisie");
		}
		
		buf.append(" au joueur ");
		
		if (target == Target.CHOOSE) {
			buf.append("choisi");
		} else {
			buf.append("suivant");
		}
		
		return buf.toString();
	}
	
	
}

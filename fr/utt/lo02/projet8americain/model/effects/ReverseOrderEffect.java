package fr.utt.lo02.projet8americain.model.effects;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Effet changeant le sens du jeu
 * @author Clement
 *
 */
public class ReverseOrderEffect extends Effect {

	public ReverseOrderEffect(String[] args) {
		super(args);
	}

	@Override
	public void trigger(Game game, Card card, Player source) {
		// Change le sens de jeu
		game.reverseDirection();
	}

	@Override
	public int getPriority() {
		return LOW;
	}

	@Override
	public String toString() {
		return "change le sens de jeu";
	}
}

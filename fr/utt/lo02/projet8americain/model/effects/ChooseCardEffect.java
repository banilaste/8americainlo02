package fr.utt.lo02.projet8americain.model.effects;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Color;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.Value;
import fr.utt.lo02.projet8americain.model.player.Askable;
import fr.utt.lo02.projet8americain.model.player.Player;
import fr.utt.lo02.projet8americain.model.settings.Settings;

/**
 * Permet au joueur de choisir par quelle carte il souhaite substituer celle qu'il vient de jouer
 * @author Clement
 *
 */
public class ChooseCardEffect extends Effect {
	private int turnApplied;
	private Value value;
	private Color color;
	
	private Value[] availableValues;
	
	public ChooseCardEffect(String[] args) throws InvalidEffectArguments {
		super(args);
		
		// On prends toutes les valeurs sauf le joker
		Value[] defaultValues = Value.values();
		availableValues = new Value[defaultValues.length - 1];
		
		for (int i = 0, j = 0; i < defaultValues.length; i++) {
			// Si la valeur n'est pas le joker
			if (defaultValues[i] != Value.JOKER) {
				// On place la valeur dans le tableau
				availableValues[j++] = defaultValues[i];
			}
		}
	}
	

	@Override
	public void trigger(Game game, Card card, Player source) {
		value = source.ask(Askable.VALUE, availableValues, card.getValue());
		color = source.ask(Askable.COLOR, Color.values(), card.getColor());
		
		// On la demande au tour suivant
		turnApplied = Settings.getTurn() + 1;
	}

	@Override
	public boolean canPlayOver(Card card, Card topCard, boolean defaultValue) {
		// Si on est bien au tour suivant
		if (Settings.getTurn() == turnApplied) {
			// Si la carte jouee est un joker
			if (card.getValue() == Value.JOKER) {
				return true;
			}
			
			// On compare la carte jouee non pasala derniere carte jouee maisacelle choisie
			return card.getValue() == value || card.getColor() == color;
		}
		
		return defaultValue;
	}


	@Override
	public int getPriority() {
		return Effect.LOW;
	}


	@Override
	public String toString() {
		return "choix d'une carte de substitution";
	}
}


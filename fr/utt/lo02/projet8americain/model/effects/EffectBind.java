package fr.utt.lo02.projet8americain.model.effects;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import fr.utt.lo02.projet8americain.model.effects.attacks.AttackEffect;
import fr.utt.lo02.projet8americain.model.effects.attacks.CounterAttackEffect;
import fr.utt.lo02.projet8americain.model.effects.attacks.CounterableAttackEffect;
import fr.utt.lo02.projet8americain.model.effects.container.ColorAppliedEffect;
import fr.utt.lo02.projet8americain.model.effects.container.PlayerNumberAppliedEffect;
import fr.utt.lo02.projet8americain.model.effects.container.SquareAppliedEffect;

/**
 * Enumeration des contructeur de chaque effet.
 * Permet de creer un effetapartir de son nom et de ses arguments
 * @author Clement
 *
 */
public enum EffectBind {
	ATTACK(AttackEffect.class),
	COUNTERABLE_ATTACK(CounterableAttackEffect.class),
	COUNTER_ATTACK(CounterAttackEffect.class),
	
	REPLAY(ReplayEffect.class),
	REVERSE_ORDER(ReverseOrderEffect.class),
	SKIP_TURN(SkipTurnEffect.class),
	
	COLOR_APPLIED(ColorAppliedEffect.class),
	PLAYER_NUMBER_APPLIED(PlayerNumberAppliedEffect.class),
	SQUARE_APPLIED(SquareAppliedEffect.class),
	
	ROTATE_HANDS(RotateHandsEffect.class),
	GIVE_CARD(GiveCardEffect.class),
	
	PLAYABLE_ON(PlayableOnEffect.class),
	PLAYABLE_ANYWHERE(PlayableAnywhereEffect.class),
	
	CHOOSE_COLOR(ChooseColorEffect.class),
	CHOOSE_CARD(ChooseCardEffect.class),
	CHOOSE_APPLY(ChooseApplyEffect.class),
	
	PLAY_WHOLE_COLOR(PlayWholeColorEffect.class),
	WIN(WinEffect.class);
	
	private Constructor<?> constructor;
	
	private EffectBind(Class<?> type) {
		try {
			constructor = type.getConstructor(String[].class);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}
	
	public Effect create(String[] args) throws InvalidEffectArguments {
		try {
			return (Effect) constructor.newInstance(new Object[] {args});
		} catch (InvocationTargetException exception) {
			// Si c'est cause par une erreur d'argument, on relance au dessus
			if (exception.getCause() instanceof InvalidEffectArguments) {
				throw (InvalidEffectArguments) exception.getCause();
				
			} else {
				// Sinon on affiche simplement
				exception.getCause().printStackTrace();
			}
		}  catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static boolean has(String action) {
		EffectBind[] bindings = EffectBind.values();
		
		for (int i = 0; i < bindings.length; i++) {
			if (bindings[i].toString().equals(action)) {
				return true;
			}
		}
		
		return false;
	}
}

package fr.utt.lo02.projet8americain.model.effects.container;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Color;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.effects.InvalidEffectArguments;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Effet applique specifie applique uniquement sur la couleur specifiee.
 * Les arguments entres sont la couleur, puis le nom de l'effect et ses arguments
 * @author Clement
 *
 */
public class ColorAppliedEffect extends EffectContainer {
	private Color color;
	
	public ColorAppliedEffect(String[] args) throws InvalidEffectArguments {
		super(args, 1);
		
		try {
			color = Color.valueOf(args[0]);
		} catch(IndexOutOfBoundsException e) {
			throw new InvalidEffectArguments(1, InvalidEffectArguments.MISSING_ARGUMENTS);
		}
	}

	@Override
	public void trigger(Game game, Card card, Player source) {
		// L'effet ne s'applique que sur la couleur
		if (card.getColor() == color) {
			super.trigger(game, card, source);
		}
	}

	@Override
	public boolean canPlayOver(Card card, Card topCard, boolean defaultValue) {
		// L'effet ne s'applique que sur la couleur
		if (card.getColor() == color) {
			return super.canPlayOver(card, topCard, defaultValue);
		}
		
		return defaultValue;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("si la carte est un ");
		
		buffer.append(color);
		buffer.append(", ");
		buffer.append(super.toString());
		
		return "si la carte est un " + color + ", ";
	}
}

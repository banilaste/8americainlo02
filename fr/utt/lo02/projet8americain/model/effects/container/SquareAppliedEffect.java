package fr.utt.lo02.projet8americain.model.effects.container;

import java.util.ArrayDeque;
import java.util.Iterator;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.Value;
import fr.utt.lo02.projet8americain.model.effects.InvalidEffectArguments;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Effet applique lorsqu'un carre de carte est joue
 * @author Clement
 *
 */
public class SquareAppliedEffect extends EffectContainer {	
	public SquareAppliedEffect(String[] args) throws InvalidEffectArguments {
		super(args, 0);
	}

	@Override
	public void trigger(Game game, Card card, Player source) {
		int number = 1;
		Value value = card.getValue();
		
		ArrayDeque<Card> discard = game.getDiscardPile();
		
		// 3 element ou plus
		if (discard.size() >= 3) {
			Iterator<Card> iter = discard.iterator();
			
			for (int i = 0; i < 3; i++) {
				// On compte le nombre de valeur identiques supplementaire
				if (iter.next().getValue() == value) {
					number ++;
				}

			}

			// Si on a finalement atteint les 4 cartes
			if (number == 4) {
				// On applique l'effet sous jacent
				super.trigger(game, card, source);
			}
		}
	}

	@Override
	public String toString() {
		return "si le 4 cartes de meme valeurs sont jouees, " + super.toString();
	}

	
}

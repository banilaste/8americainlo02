package fr.utt.lo02.projet8americain.model.effects.container;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.Value;
import fr.utt.lo02.projet8americain.model.effects.Effect;
import fr.utt.lo02.projet8americain.model.effects.EffectBind;
import fr.utt.lo02.projet8americain.model.effects.InvalidEffectArguments;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Effect contenant un autre effet
 * @author Clement
 *
 */
public abstract class EffectContainer extends Effect {
	private Effect innerEffect;

	/**
	 * Cree un effet contenant un autre effet. Les arguments sont utilisesapartir de l'offset donne
	 * @param args arguments de l'effet
	 * @param offset decalage dans les arguments
	 * @throws InvalidEffectArguments
	 */
	public EffectContainer(String[] args, int offset) throws InvalidEffectArguments {
		super(args);
		try {
			String effectName = args[offset];
			String[] effectArgs = new String[args.length - 1 - offset];
			
			
			for (int i = 1; i < args.length - offset; i++) {
				effectArgs[i - 1] = args[i + offset];
			}

			// On cree l'effet interne avec les arguments specifies
			try {
				innerEffect = EffectBind.valueOf(effectName).create(effectArgs);

			} catch (IllegalArgumentException e) {
				throw new InvalidEffectArguments(effectName, InvalidEffectArguments.INVALID_EFFECT);
			}
		} catch(IndexOutOfBoundsException e) {
			throw new InvalidEffectArguments(offset + 1 - args.length, InvalidEffectArguments.MISSING_ARGUMENTS);
		}
	}

	@Override
	public void init(Game game, Value appliedOn) {
		innerEffect.init(game, appliedOn);
	}

	@Override
	public void trigger(Game game, Card card, Player source) {
		innerEffect.trigger(game, card, source);
	}

	@Override
	public boolean canPlayOver(Card card, Card topCard, boolean defaultValue) {
		return innerEffect.canPlayOver(card, topCard, defaultValue);
	}

	@Override
	public int getPriority() {
		// On prends la priorite de l'effet contenu
		return innerEffect.getPriority();
	}
	
	public String toString() {
		return innerEffect.toString();
	}
}

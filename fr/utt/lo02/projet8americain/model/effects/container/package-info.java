/**
 * Package contenant les effets associes a un sous effet.
 * Ce dernier peut etre execute conditionnellement au
 * contexte ou associe a d'autres effets
 * @author Clement
 *
 */
package fr.utt.lo02.projet8americain.model.effects.container;
package fr.utt.lo02.projet8americain.model.effects.container;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.effects.InvalidEffectArguments;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Applique un effet decrit en argument lorsque le nombre de joueur est inferieur ou egal
 * au nombre de joueurs donne.
 * @author Clement
 *
 */
public class PlayerNumberAppliedEffect extends EffectContainer {
	private int maxPlayers = 0;
	
	public PlayerNumberAppliedEffect(String[] args) throws InvalidEffectArguments {
		super(args, 1);
		
		try {
			maxPlayers = Integer.parseInt(args[0]);
		} catch(IndexOutOfBoundsException e) {
			throw new InvalidEffectArguments(2 - args.length, InvalidEffectArguments.MISSING_ARGUMENTS);
		}
	}
	
	@Override
	public void trigger(Game game, Card card, Player source) {
		// L'effet ne s'applique que sur le bon nombre de joueurs
		if (canApply()) {
			super.trigger(game, card, source);
		}
	}

	@Override
	public boolean canPlayOver(Card card, Card topCard, boolean defaultValue) {
		// L'effet ne s'applique que sur le bon nombre de joueurs
		if (canApply()) {
			return super.canPlayOver(card, topCard, defaultValue);
		}
		
		return defaultValue;
	}
	
	private boolean canApply() {
		int count = 0;
		Player[] players = Game.getInstance().getPlayers();
		
		for (int j = 0; j < players.length; j++) {
			if (players[j].isPlaying()) {
				count ++;
			}
		}
		
		return count < maxPlayers;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("si le nombre de joueur est inferieura");
		buffer.append(maxPlayers);
		buffer.append(", ");
		buffer.append(super.toString());
		
		return null;
	}
}

package fr.utt.lo02.projet8americain.model.effects;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Color;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.Value;
import fr.utt.lo02.projet8americain.model.player.Askable;
import fr.utt.lo02.projet8americain.model.player.Player;
import fr.utt.lo02.projet8americain.model.settings.Settings;

/**
 * Change la couleur devant etre jouee selon la demande de l'utilisateur
 * @author Clement
 *
 */
public class ChooseColorEffect extends Effect {
	private int turnApplied;
	private Color color;
	
	public ChooseColorEffect(String[] args) throws InvalidEffectArguments {
		super(args);
	}
	

	@Override
	public void trigger(Game game, Card card, Player source) {
		color = source.ask(Askable.COLOR, Color.values(), card.getColor());
		
		// On la demande au tour suivant
		turnApplied = Settings.getTurn() + 1;
	}

	@Override
	public boolean canPlayOver(Card card, Card topCard, boolean defaultValue) {
		// (Non fonctionnel)
		
		// Si on est bien au tour suivant
		if (Settings.getTurn() == turnApplied) {
			if (card.getValue() == Value.JOKER) {
				return true;
			}
			
			// On compare la carte jouee non pasala derniere carte jouee maisacelle choisie
			return topCard.getValue() == card.getValue() || card.getColor() == color;
		}
		
		return defaultValue;
	}


	@Override
	public int getPriority() {
		return Effect.NORMAL;
	}


	@Override
	public String toString() {
		return "change la couleur";
	}
}

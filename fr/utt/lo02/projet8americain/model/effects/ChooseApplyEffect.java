package fr.utt.lo02.projet8americain.model.effects;

import java.util.ArrayList;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.player.Askable;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Donne le choix au joueur d'applique ou non l'effet, ou le choix entre dexu effets
 * @author Clement
 *
 */
public class ChooseApplyEffect extends Effect {
	private final static String SEPARATOR = "|";
	
	private Effect[] innerEffects;
	
	public ChooseApplyEffect(String[] args) throws InvalidEffectArguments {
		super(args);
		
		ArrayList<Effect> effects = new ArrayList<Effect>();
		String effectName = null;
		ArrayList<String> effectArgs = new ArrayList<String>();

		for (int i = 0; i < args.length; i++) {
			// Separateur -> fin d'un effet, debut d'un autre
			if (args[i].equals(SEPARATOR)) {
				effects.add(getEffect(effectName, effectArgs));
				
				effectArgs.clear();
				effectName = null;
			}
			
			// si pas declare, le nom est rempli
			else if (effectName == null) {
				effectName = args[i];
			}
			
			// sinon ce sont d'autres arguments
			else {
				effectArgs.add(args[i]);
			}
		}
		
		// On ajoute le dernier
		effects.add(getEffect(effectName, effectArgs));
		
		// On recupere les tableaux (minimum deux choix)
		innerEffects = new Effect[Math.min(effects.size(), 2)];
		
		// On veux au moins un effet
		if (effects.size() == 0) {
			throw new InvalidEffectArguments(1, InvalidEffectArguments.MISSING_ARGUMENTS);
		}
		
		// On copie les elements dedans
		// Le dernier sera misanull si il y a trop de place (equivautane pas applique l'effet)
		effects.toArray(innerEffects);
	}

	@Override
	public void trigger(Game game, Card card, Player source) {
		Effect effect = source.ask(Askable.EFFECT, innerEffects, innerEffects[0]);

		// Si l'effet est non nul, on l'applique
		if (effect != null) {
			effect.trigger(game, card, source);
		}
	}
	
	/**
	 * Renvoie un effet creeapartir de ses arguments
	 * @param name nom de l'effet
	 * @param args arguments
	 * @return effet instancie
	 * @throws InvalidEffectArguments
	 */
	private Effect getEffect(String name, ArrayList<String> args) throws InvalidEffectArguments {
		// On copie les arguments dans un tableau
		String argsArray[] = new String[args.size()];
		args.toArray(argsArray);
		
		try {
			// Et on cree l'effet
			return EffectBind.valueOf(name).create(argsArray);
		} catch(IllegalArgumentException e) {
			throw new InvalidEffectArguments(name, InvalidEffectArguments.INVALID_EFFECT);
		}
	}

	@Override
	public int getPriority() {
		// On renvoie la moyenne de la priorite des actions
		int priority = 0;
		
		for (int i = 0; i < innerEffects.length; i++) {
			if (innerEffects[i] != null) {
				priority += innerEffects[i].getPriority();
			}
		}
		
		if (innerEffects[innerEffects.length - 1] == null) {
			priority /= innerEffects.length - 1;
		} else {
			priority /= innerEffects.length;
		}
		
		return priority;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("choix entre : ");
		
		for (int i = 0; i < innerEffects.length; i++) {
			// Affichage des effets
			if (innerEffects[i] != null) {
				buffer.append(innerEffects[i].toString());
			} else {
				buffer.append("ne rien faire");
			}
			
			// Et des separateurs
			if (i == innerEffects.length - 2) {
				buffer.append(" et ");
			} else if (i < innerEffects.length - 2) {
				buffer.append(", ");
			}
		}

		return buffer.toString();
	}
}

package fr.utt.lo02.projet8americain.model.variants;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.Value;
import fr.utt.lo02.projet8americain.model.effects.Effect;
import fr.utt.lo02.projet8americain.model.player.Player;
import fr.utt.lo02.projet8americain.model.pointcounter.PointCounter;

/**
 * Classe representant une variante
 * @author Clement
 *
 */
public class Variant {
	public static HashMap<String, Variant> loadedVariants = new HashMap<String, Variant>();

	private HashMap<Value, Effect[]> effects;
	private PointCounter countingMethod;

	private String name;

	private boolean loaded = false;
	
	/**
	 * Cree une variante non chargee (non utilisable)
	 * @param name nom de la variante
	 */
	private Variant(String name) {
		this.name = name;

		// On ajoute la variante aux variante "chargees"
		loadedVariants.put(name, this);
	}
	
	/**
	 * Cree une variante chargee.
	 * 
	 * Constructeur en visibilite package car on doit passer par un parseur de variante
	 * ou une classe associee.
	 * 
	 * @see AlternativeVariantParser
	 * 
	 * @param name nom de la variante
	 * @param effectMap effets de la variante
	 * @param pointCounter methode de comptage (facultative)
	 */
	Variant(String name, HashMap<Value, Effect[]> effectMap, PointCounter pointCounter) {
		this(name);
		this.loaded = true;
		this.effects = effectMap;
		this.countingMethod = pointCounter;

		// On prends tous les effets
		Iterator<Effect[]> iter = effectMap.values().iterator();
		
		while(iter.hasNext()) {
			// On trie les effets par priorite
			Arrays.sort(iter.next(), new Comparator<Effect>() {
				@Override
				public int compare(Effect a, Effect b) {
					// Tri dans l'ordre decroissant
					return b.getPriority() - a.getPriority();
				}
			});
		}

	}

	/**
	 * Applique les effets de la carte jouee
	 * @param source Joueur posant la carte
	 * @param card Carte posee
	 * @param game Partie courantes
	 */
	public void onCardPlayed(Player source, Card card, Game game) {
		Effect[] cardEffects;

		//Si des effets particuliers sont associés à cette carte, on les applique
		cardEffects = getEffects(card.getValue());

		for (int i = 0; i < cardEffects.length; i++) {
			cardEffects[i].trigger(game, card, source);
		}
	}

	/**
	 * Verifie que cette variante autorise de jouer card sur topCard
	 * @param cardPlayed carte voulant etre posee
	 * @param topCard derniere carte jouee
	 * @return booleen indiquant si la carte peut etre jouee
	 */
	public boolean canPlayOver(Card cardPlayed, Card topCard) {
		boolean defaultValue;
		boolean value;
		Effect[] cardEffects;

		// Cartes nulles (???)
		if (cardPlayed == null || topCard == null) {
			throw new NullPointerException("bad use of canPlayOver (only instancied cards)");
		}

		// Booleen indiquant la possibilite de joueur ou non selon les regles de base
		defaultValue = cardPlayed.getColor() == topCard.getColor() || cardPlayed.getValue() == topCard.getValue();

		// Le joker est une carte neutre par defaut
		if (cardPlayed.getValue() == Value.JOKER || topCard.getValue() == Value.JOKER) {
			defaultValue = true;
		}

		// Valeur reele qui sera utilisee
		value = defaultValue;

		cardEffects = getEffects(cardPlayed.getValue());

		// On demande aux effets si la carte peut etre jouee
		for (int i = 0; i < cardEffects.length; i++) {
			value = cardEffects[i].canPlayOver(cardPlayed, topCard, defaultValue);

			if (value != defaultValue) {
				return value;
			}
		}

		return value;
	}

	/**
	 * Renvoie les effets associesaune carte
	 * @param value valeur de la carte
	 * @return effets associes
	 */
	public Effect[] getEffects(Value value) {
		if (effects.containsKey(value)) {
			return effects.get(value);
		} else {
			return new Effect[0];
		}
	}

	/**
	 * Renvoie les noms de fichiers de toutes les variantes
	 * @return tableau contenant les noms de fichier
	 */
	public static String[] getFileNames() {
		return AlternativeVariantParser.getVariantsNames();
	}
	
	/**
	 * Renvoie une variante selon son nom
	 * @param name nom de la variante
	 * @return variante associee au nom
	 * @throws InvalidVariantFileException 
	 * @throws IOException 
	 */
	public static Variant get(String name) throws InvalidVariantFileException, IOException {
		if (loadedVariants.containsKey(name) && loadedVariants.get(name).isLoaded()) {
			return loadedVariants.get(name);
		}

		// La variante sera ajoutee automatiquementaloadedVariants
		return AlternativeVariantParser.load(name);
	}

	/**
	 * Renvoie le nom d'une variante
	 * @param fileName nom du fichier de la variante
	 * @return nom de la variante
	 * @throws IOException 
	 */
	public static String getName(String fileName) throws IOException {
		if (!loadedVariants.containsKey(fileName)) {
			// On ne charge que le nom du fichier
			String name = AlternativeVariantParser.loadName(fileName);
			
			// Et on le place dans une variante non chargee
			loadedVariants.put(fileName, new Variant(name));
		}

		return loadedVariants.get(fileName).getName();
	}

	/*
	 * Getters
	 */
	public PointCounter getCountingMethod() {
		return countingMethod;
	}

	public boolean hasCountingMethod() {
		return countingMethod != null;
	}

	public String getName() {
		return name;
	}

	public String getFileName() {
		return name;
	}
	
	public boolean isLoaded() {
		return loaded;
	}
}

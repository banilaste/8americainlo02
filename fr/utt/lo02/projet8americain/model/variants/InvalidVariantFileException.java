package fr.utt.lo02.projet8americain.model.variants;

/**
 * Exception indiquant un probleme de donnee dans un fichier de variante
 * @author Clement
 *
 */
@SuppressWarnings("serial")
public class InvalidVariantFileException extends Exception {
	public InvalidVariantFileException(String message) {
		super(message);
	}
	
}

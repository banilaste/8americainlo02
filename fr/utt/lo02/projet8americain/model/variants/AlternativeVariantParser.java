package fr.utt.lo02.projet8americain.model.variants;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import fr.utt.lo02.projet8americain.model.Value;
import fr.utt.lo02.projet8americain.model.effects.Effect;
import fr.utt.lo02.projet8americain.model.effects.EffectBind;
import fr.utt.lo02.projet8americain.model.effects.InvalidEffectArguments;
import fr.utt.lo02.projet8americain.model.pointcounter.NegativeCounter;
import fr.utt.lo02.projet8americain.model.pointcounter.PointCounter;

/**
 * Classe permettant de creer une variante a partir d'un fichier de specification de variante
 * @author nathansoufflet
 *
 */
public class AlternativeVariantParser {
	/**
	 * Dossier contenant les variantes
	 */
	private final static String DEFAULT_FOLDER = "Variants/alt/";
	
	/**
	 * Separateur entre les effets et les points associes aux cartes
	 */
	private final static String COUTING_METHOD_SEPATATOR = "==COUNTING==";
	
	/**
	 * Extension des fichiers
	 */
	private final static String EXT = ".alt";

	/**
	 * Recupere les effets dans le fichier
	 * @param reader reader sur le fichier de la variante
	 * @return effets et leur valeur associee de la variante
	 * @throws InvalidVariantFileException
	 * @throws IOException
	 */
	private static HashMap<Value, Effect[]> parseEffects(BufferedReader reader) throws InvalidVariantFileException, IOException {
		HashMap<Value, Effect[]> bindedEffects = new HashMap<Value, Effect[]>();
		ArrayList<Effect> effects = new ArrayList<Effect>();
		Effect effectArray[];
		
		String args[];
		String effectName, line;
		Value value = null;
		
		line = getLine(reader);
		
		while (line != null && !line.trim().equals(COUTING_METHOD_SEPATATOR)) {
			// Debut par une tabulation ou un espace -> effet d'une carte
			if (line.startsWith("\t") || line.startsWith(" ")) {
				line = line.trim();
				
				// On separe le nom de l'effet de ses arguments
				args = line.split(" ", 2);
				
				// On recupere les parties
				effectName = args[0];
				
				if (args.length > 1) {
					args = args[1].split(" ");
				} else {
					args = new String[0];
				}
				
				// Puis l'effet qu'on ajoute au reste
				try {
					effects.add(EffectBind.valueOf(effectName).create(args));
				} catch(IllegalArgumentException e) {
					throw new InvalidVariantFileException(effectName + " is not a valid effect name");
					
				} catch (InvalidEffectArguments e) {
					throw new InvalidVariantFileException("[" + effectName + "] " + e.getMessage());
				}
			}
			
			// Sinon nouvelle carte
			else {
				// On garde l'ancienne si elle existe
				if (value != null) {
					effectArray = new Effect[effects.size()];
					effectArray = effects.toArray(effectArray);
					
					bindedEffects.put(value, effectArray);
					
					// On vide les effets
					effects.clear();
				}
				
				// et on change de valeur
				try {
					value = Value.valueOf(line.trim());
				} catch(IllegalArgumentException e) {
					throw new InvalidVariantFileException(line.trim() + " is not a valid value name");
				}
			}
			
			line = getLine(reader);
		}
		
		// On ajoute finalement le dernier effet
		if (value != null) {
			effectArray = new Effect[effects.size()];
			effectArray = effects.toArray(effectArray);
			
			bindedEffects.put(value, effectArray);
		}
		
		return bindedEffects;
	}
	
	/**
	 * Renvoie une ligne en ignorant les commentaires et lignes vides
	 * @param reader lecteur du fichier
	 * @return ligne non vide ni commentee
	 * @throws IOException
	 */
	private static String getLine(BufferedReader reader) throws IOException {
		String line = reader.readLine();

		while (line != null && (line.trim().length() == 0 || line.startsWith("#"))) {
			line = reader.readLine();
		}
		
		return line;
	}

	/**
	 * Renvoie une methode de comptage eventuelle associeeal'effet.
	 * Null sera renvoye si celle-ci n'est pas presente
	 * @param br objet de lecture du fichier de variante
	 * @return methode de comptage des points
	 * @throws InvalidVariantFileException 
	 * @throws IOException 
	 */
	private static PointCounter parseCountingMethod(BufferedReader br) throws InvalidVariantFileException, IOException {
		String line = getLine(br), parts[];
		NegativeCounter counter = null;
		HashMap<Value, Integer> points = new HashMap<Value, Integer>();
		
		// Tant qu'il reste des lignesalire
		while(line != null) {
			parts = line.trim().split(" ", 2);
			
			if (parts.length < 2) {
				throw new InvalidVariantFileException("no points associated with " + parts[0]);
			}
			
			// On ajoute les points associesaune valeur
			try {
				points.put(Value.valueOf(parts[0]), Integer.parseInt(parts[1]));
				
			} catch(NumberFormatException e){
				throw new InvalidVariantFileException(parts[1] + " is not a valid number");
				
			} catch(IllegalArgumentException e) {				
				throw new InvalidVariantFileException(parts[0] + " is not a valid value name");
			}
			
			
			line = getLine(br);
		}
		
		// S'il y a au moins un point, on cree la methode
		if (points.size() > 0) {
			counter = new NegativeCounter(points);
		}
		
		// Sinon null sera renvoye
		return counter;
	}
	
	/**
	 * Renvoie le chemin du fichier selon son nom
	 * @param name nom du fichier
	 * @return chemin vers le fichier
	 */
	private static String getPath(String name) {
		return DEFAULT_FOLDER + name + EXT;
	}
	
	/**
	 * Renvoie une variante depuis le fichier specifie
	 * @param filePath nom du fichier
	 * @return variante chargee
	 */
	static Variant fromFile(String filePath, String fileName) throws IOException, InvalidVariantFileException {
		HashMap<Value, Effect[]> effects;
		String variantName;
		BufferedReader br = new BufferedReader(new FileReader(new File(filePath)));
		PointCounter countingMethod;
		
		// On recupere le nom de la variante
		variantName = getLine(br);
		
		// Les effets
		effects = parseEffects(br);
		
		// Et l'eventuelle alternative de comptage des points
		countingMethod = parseCountingMethod(br);
		
		br.close();

		return new Variant(variantName, effects, countingMethod);
	}

	/**
	 * Renvoie une variante selon son nom dans le dossier par defaut
	 * @param name nom de la variante
	 * @return variante associee
	 */
	static Variant load(String name) throws IOException, InvalidVariantFileException {
		// On renvoie une variante chargee
		return fromFile(getPath(name), name);
	}

	/**
	 * Renvoie la liste des variantes parsables presentes dans le dossier par defaut
	 * @return nom des fichiers contenant les variantes
	 */
	static String[] getVariantsNames() {
		String names[] = new File(DEFAULT_FOLDER).list(new FilenameFilter() {
			// On prends seulement les noms terminant par .alt
			public boolean accept(File dir, String name) {
				return name.endsWith(EXT);
			}
		});

		// On enleve les .alt des noms de variantes
		for (int i = 0; i < names.length; i++) {
			names[i] = names[i].substring(0, names[i].length() - EXT.length());
		}

		// Et on les renvoies
		return names;
	}

	/**
	 * Renvoie le nom d'une variante definie dans son fichier
	 * @param fileName nom du fichier
	 * @return nom de la variante
	 * @throws IOException 
	 */
	static String loadName(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(new File(getPath(fileName))));
		String variantName = getLine(br);
		
		br.close();
		
		return variantName;
	}
}

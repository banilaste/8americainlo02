/**
 * Package contenant les classe pour la gestion des variantes (creation, recuperation, erreurs).
 * 
 * @see fr.utt.lo02.projet8americain.model.variants.Variant
 */
package fr.utt.lo02.projet8americain.model.variants;
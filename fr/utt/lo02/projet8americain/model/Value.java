package fr.utt.lo02.projet8americain.model;

/**
 * Representation de la valeur d'une carte. Chaque valeur
 * contient sa description textuelle et son nombre de points associe
 * @author Clement
 *
 */
public enum Value {
    C1("1", 50),
    C2("2", 20),
    C3("3", 3),
    C4("4", 4),
    C5("5", 5),
    C6("6", 6),
    C7("7", 20),
    C8("8", 50),
    C9("9", 9),
    C10("10", 20),
    JACK("V", 20),
    QUEEN("D", 10),
    KING("R", 10),
    JOKER("J", 0);
	
	private String desc;
	private int defaultPoints;
	
	private Value(String desc, int defaultPoints) {
		this.desc = desc;
		this.defaultPoints = defaultPoints;
	}
	
	/**
	 * Renvoie le nombre de points par defaut donne par cette valeur
	 * @return nombre de points
	 */
	public int getDefaultPoints() {
		return defaultPoints;
	}
	
	public String toString() {
		return desc;
	}
	
	/**
	 * Renvoie la valeur associeeala chaine de description ou null si joker.
	 * @param str chaine decrivant la valeur
	 * @return valeur de la carte ou null (joker)
	 * @throws IllegalArgumentException exception levee en cas de description invalide
	 */
	public static Value fromString(String str) throws IllegalArgumentException {
		for (Value v : Value.values()) {
			if (v.toString().equals(str.toUpperCase())) {
				return v;
			}
		}
		
		throw new IllegalArgumentException();
	}
}

package fr.utt.lo02.projet8americain.model;

import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Classe d'ecoute des actions utilisateur
 * @author Clement
 *
 */
public interface GameActionListener {
	/**
	 * Methode appellee lorsqu'une carte est jouee
	 * @param source joueur posant la carte
	 * @param card carte jouee
	 */
	public void onCardPlayed(Player source, Card card);
	
	/**
	 * Methode appellee lorsqu'un joueur pioche une carte
	 * @param source joueur recuperant la carte
	 * @param cards cartes piochees
	 */
	public void onCardsPicked(Player source, Card[] cards);
}

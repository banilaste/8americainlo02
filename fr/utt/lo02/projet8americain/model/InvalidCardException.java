package fr.utt.lo02.projet8americain.model;

/**
 * Exception levee lorsqu'une mauvaise carte est donnee
 * @author Clement
 *
 */
public class InvalidCardException extends Exception {
	private static final long serialVersionUID = 1L;

	public InvalidCardException() {
		super("carte invalide");
	}
	
	public InvalidCardException(String message) {
		super(message);
	}


}

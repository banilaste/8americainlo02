package fr.utt.lo02.projet8americain.model.player;

import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.player.strategy.Strategy;
import fr.utt.lo02.projet8americain.model.player.strategy.StrategyInstruction;
import fr.utt.lo02.projet8americain.model.settings.Setting;

/**
 * Classe representant un joueur virtuel. Ce joueur se voit associer une strategie et
 * doit l'appliquer
 * 
 * @author Clement
 *
 */
public class VirtualPlayer extends Player implements Runnable {
	private final static String names[] = {"Jean", "Camille", "Michel", "Andre", "Louis"};
	private static int namesIndex = 0;

	private Object lock;
	private Strategy strategy;

	private Thread thread;

	public VirtualPlayer(Strategy strategy) {
		// On appelle le constructeur d'un joueur
		super(names[((namesIndex++) % names.length)]);

		this.strategy = strategy;

		// Creation de l'objet de verrouillage
		lock = new Object();

		// Demarrage du thread du joueur associe
		thread = new Thread(this);
		thread.start();

	}

	public void setCanPlay(boolean canPlay, boolean isTurn) {
		super.setCanPlay(canPlay, isTurn);
		
		// On autoriseajouer dans le thread
		if (canPlay) {
			synchronized (lock) {
				lock.notify();
			}
		}
	}

	public void run() {
		VirtualPlayer self = this;

		while (true) {
			try {
				// On attend d'avoir le droit de jouer
				synchronized (lock) {
					while (!this.canPlay()) {
						lock.wait();
					}
				}
				
				// On prends le lock sur soi meme
				synchronized (this) {
					// On attend un peu si le mode permet de jouer simultanement
					if (Setting.VIRTUAL_PLAYER_DELAY.getBoolean()) {
						// Une longueur aleatoire
						Thread.sleep((long) (500 + Math.random() * 500));
					}
	
					// Puis on joue
					StrategyInstruction instruction = this.strategy.apply(this.getHand(), Game.getInstance().getDiscardPile(), isTurn());
	
					switch (instruction.getType()) {
					case PICK:
						self.skip();
						break;
	
					case PLAY:
						self.play(instruction.getCardPlayed());
						break;
	
					default:
						break;
					}
				}
			} catch(InterruptedException e) {
				// On arrete le thread definitivement
				thread.interrupt();
			}

		}
	}

	@Override
	public <E> E ask(Askable description, E[] availableValues, E defaultValue) {
		return strategy.asked(description, availableValues, defaultValue);
	}
}

package fr.utt.lo02.projet8americain.model.player.strategy;

import java.util.ArrayDeque;
import java.util.LinkedList;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.effects.Effect;
import fr.utt.lo02.projet8americain.model.player.Askable;
import fr.utt.lo02.projet8americain.model.settings.Settings;

/**
 * Strategie normale pour un joueur virtuel
 * @author Clement
 *
 */
public class NormalStrategy implements Strategy {
	private static NormalStrategy instance;

	private NormalStrategy() {}

	@Override
	public StrategyInstruction apply(LinkedList<Card> hand, ArrayDeque<Card> discardPile, boolean mustPlay) {
		// On prends la derniere carte jouee
		Card lastPlayed = discardPile.element();

		// On cherche une carte jouable dessus
		for (Card card : hand) {
			// Si elle est jouable dessus
			if (Settings.getVariant().canPlayOver(card, lastPlayed)) {
				// On la renvoie direct
				return new StrategyInstruction(InstructionType.PLAY, card);
			}
		}

		// Sinon on pioche si on est oblige de jouer, ou on passe si ce n'est pas le cas
		return new StrategyInstruction(InstructionType.PICK, null);
	}

	@Override
	public <E> E asked(Askable description, E[] availableValues, E defaultValue) {
		// Si on demande de choisir un effet, on regarde si la valeur null est presente
		if (defaultValue instanceof Effect) {
			// Elle est generalement presenteala fin
			if (availableValues[availableValues.length - 1] == null) {
				// On renvoie alors l'autre choix
				return availableValues[0];
			}
		}

		// On renvoie une valeur au pif dans les valeurs dispos
		return availableValues[(int) (availableValues.length * Math.random())];
	}

	/**
	 * Renvoie le singleton
	 * @return instance de strategie normale
	 */
	public static Strategy get() {
		if (instance == null) {
			instance = new NormalStrategy();
		}

		return instance;
	}

}

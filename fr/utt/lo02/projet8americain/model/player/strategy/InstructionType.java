package fr.utt.lo02.projet8americain.model.player.strategy;

/**
 * Type d'instruction donnee par une strategie
 * @author Clement
 *
 */
public enum InstructionType {
	PICK, PLAY
}

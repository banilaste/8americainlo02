package fr.utt.lo02.projet8americain.model.player.strategy;

import java.util.ArrayDeque;
import java.util.LinkedList;
import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.effects.Effect;
import fr.utt.lo02.projet8americain.model.player.Askable;
import fr.utt.lo02.projet8americain.model.settings.Settings;

/**
 * Strategie basique consistantajouer la premiere carte possible.
 * 
 * Les possibilites offertes au joueur virtuel (jouer hors de son tour,
 * appliquer un effet) sont annulees.
 * 
 * @author Clement
 *
 */
public class EasyStrategy implements Strategy {
	private static EasyStrategy instance;
	
	private EasyStrategy() {}
	
	@Override
	public StrategyInstruction apply(LinkedList<Card> hand, ArrayDeque<Card> discardPile, boolean mustPlay) {
		// Si ce n'est pas notre tour, on n'essaie meme pas de jouer
		if (!mustPlay) {
			return new StrategyInstruction(InstructionType.PICK, null);
		}
		
		// On prends la derniere carte jouee
		Card lastPlayed = discardPile.element();
		
		// On cherche une carte jouable dessus
		for (Card card : hand) {
			// Si elle est jouable dessus
			if (Settings.getVariant().canPlayOver(card, lastPlayed)) {
				// On la renvoie direct
				return new StrategyInstruction(InstructionType.PLAY, card);
			}
		}
		
		// Sinon on pioche si on est oblige de jouer, ou on passe si ce n'est pas le cas
		return new StrategyInstruction(InstructionType.PICK, null);
	}

	@Override
	public <E> E asked(Askable description, E[] availableValues, E defaultValue) {
		// Si on demande de choisir un effet, on regarde si la valeur null est presente
		if (defaultValue instanceof Effect) {
			// Elle est generalement presenteala fin
			if (availableValues[availableValues.length - 1] == null) {
				return null;
			}
		}
		
		// On renvoie la valeur par defaut sinon
		return defaultValue;
	}

	/**
	 * Renvoie le singleton
	 * @return instance de strategie simple
	 */
	public static Strategy get() {
		if (instance == null) {
			instance = new EasyStrategy();
		}
		
		return instance;
	}

}

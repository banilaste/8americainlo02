package fr.utt.lo02.projet8americain.model.player.strategy;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Color;
import fr.utt.lo02.projet8americain.model.player.Askable;
import fr.utt.lo02.projet8americain.model.player.Player;
import fr.utt.lo02.projet8americain.model.settings.Settings;
import fr.utt.lo02.projet8americain.model.variants.Variant;

/**
 * Strategie plus evoluee
 * @author Clement
 *
 */
public class HardStrategy implements Strategy {
	private static HardStrategy instance;

	private HardStrategy() {}
	
	@Override
	public StrategyInstruction apply(LinkedList<Card> hand, ArrayDeque<Card> arrayDeque, boolean mustPlay) {
		// On compte les couleurs
		ArrayList<Card> playableCards = new ArrayList<Card>();

		HashMap<Color, Integer> colorCount = new HashMap<Color, Integer>();
		Variant variant = Settings.getVariant();
		Card lastCard = arrayDeque.element();

		// Misea0 (utile?)
		for (int i = 0; i < Color.values().length; i++) {
			colorCount.put(Color.values()[i], 0);
		}

		Iterator<Card> iter = hand.iterator();
		Card next;

		while(iter.hasNext()) {
			next = iter.next();

			// Si la carte peut etre jouee
			if (variant.canPlayOver(next, lastCard)) {
				playableCards.add(next);
			}

			// On augmente le nombre de compte
			colorCount.put(next.getColor(), colorCount.get(next.getColor()));
		}

		if (playableCards.size() >= 2) {
			// On prends la couleur avec le maximum de cartes
			playableCards.sort(new Comparator<Card>() {
				@Override
				public int compare(Card card1, Card card2) {
					return colorCount.get(card2.getColor()) - colorCount.get(card1.getColor());
				}
			});

			return new StrategyInstruction(InstructionType.PLAY, playableCards.get(0));
		} else if (playableCards.size() > 1) {
			return new StrategyInstruction(InstructionType.PLAY, playableCards.get(0));
		} else {
			return new StrategyInstruction(InstructionType.PICK, null);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <E> E asked(Askable description, E[] availableValues, E defaultValue) {
		int size = availableValues.length;

		// Si on demande un joueur
		if (description == Askable.PLAYER) {
			// On cible celui avec le moins de cartes
			Player[] players = (Player[]) availableValues;
			Player min = players[0];

			for (int i = 1; i < players.length; i++) {
				if (players[i].getHand().size() < min.getHand().size()) {
					min = players[i];
				}
			}

			// On le reconvertit
			return (E) min;

		} else if (description == Askable.EFFECT) {
			// On renvoie un effet aleatoire dans ceux non nuls
			if (availableValues[size - 1] == null) {
				size -= 1;
			}


		}

		// On renvoie une valeur aleatoire dans les possibilites restantes
		return availableValues[(int) (Math.random() * size)];
	}

	/**
	 * Renvoie le singleton
	 * @return instance de strategie complexe
	 */
	public static Strategy get() {
		if (instance == null) {
			instance = new HardStrategy();
		}

		return instance;
	}

}

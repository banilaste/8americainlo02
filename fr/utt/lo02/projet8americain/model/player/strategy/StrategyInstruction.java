package fr.utt.lo02.projet8americain.model.player.strategy;

import fr.utt.lo02.projet8americain.model.Card;

/**
 * Classe contenant l'instruction renvoyee par une strategie
 * @author Clement
 *
 */
public class StrategyInstruction {
	private InstructionType type;
	private Card cardPlayed;
	
	public StrategyInstruction(InstructionType type, Card card) {
		this.type = type;
		this.cardPlayed = card;
	}
	
	public InstructionType getType() {
		return type;
	}

	public Card getCardPlayed() {
		return cardPlayed;
	}
}

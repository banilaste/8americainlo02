package fr.utt.lo02.projet8americain.model.player.strategy;

import java.util.ArrayDeque;
import java.util.LinkedList;
import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.player.Askable;

/**
 * Interface representant la strategie d'un joueur, c'estadire sa reponse face e
 * des demandes de valeur et la carte qu'il souhaite jouer
 * @author Clement
 *
 */
public interface Strategy {
	/**
	 * Applique la strategie selon la main possede et la pile de jeu
	 * @param hand Main du joueur virtuel
	 * @param discardPile Defausse
	 * @param mustPlay Si le joueur doit absolument passer
	 * @return Une instruction de jeu,aappliquer si le tour est encore au joueur
	 */
    public StrategyInstruction apply(LinkedList<Card> hand, ArrayDeque<Card> discardPile, boolean mustPlay);

    /**
     * Renvoie un objet demande
     * @param description Description du type d'objet demande
     * @param availableValues Valeurs disponibles
     * @param defaultValue Valeur par defaut
     * @return valeur demandee
     */
	public <E> E asked(Askable description, E[] availableValues, E defaultValue);

}

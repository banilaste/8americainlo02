/**
 * Package contenant les elements de la strategie d'un joueur virtuel
 * @author Clement
 *
 */
package fr.utt.lo02.projet8americain.model.player.strategy;
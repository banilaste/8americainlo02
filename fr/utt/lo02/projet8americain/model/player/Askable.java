package fr.utt.lo02.projet8americain.model.player;

/**
 * Enumeration des valeurs pouvant etre demandees au joueur.
 * A chaque valeur est associe une description textuelle.
 * 
 * @author Clement
 *
 */
public enum Askable {
	VALUE("une valeur de carte"), PLAYER("un joueur"), COLOR("une couleur"), CARD("une carte"), EFFECT("un effet");
	
	private String description;

	private Askable(String desc) {
		this.description = desc;
	}
	
	public String toString() {
		return description;
	}
}

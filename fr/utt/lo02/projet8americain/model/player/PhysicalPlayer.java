package fr.utt.lo02.projet8americain.model.player;

/**
 * Classe representant un joueur reel
 * @author Clement
 *
 */
public class PhysicalPlayer extends Player {
	private AskRequest<?> request = null;
	
	public PhysicalPlayer(String name) {
		super(name);
	}

	@Override
	public <E> E ask(Askable description, E[] availableValues, E defaultValue) {
		// On garde les parametres entres
		AskRequest<E> request = new AskRequest<E>(description, availableValues, defaultValue, this);
		
		this.request = request;
		
		// On notifie la vue
		this.setChanged();
		this.notifyObservers();
		
		// On attend la reponse de l'utilisateur
		synchronized (this) {
			try {
				// Tant que le choix n'est pas defini
				while (!request.hasChosen()) {
					this.wait();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		// On supprime la requete globalement
		this.request = null;
		
		// Puis on renvoie la valeur recuperee sur la locale
		return request.getChoice();
	}

	public boolean isWaitingChoice() {
		return request != null;
	}
	
	public AskRequest<?> getRequest() {
		return request;
	}
}

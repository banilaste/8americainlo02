package fr.utt.lo02.projet8americain.model.player;

/**
 * Objet representant une requete de donneesal'utilisateur
 * @author Clement
 *
 * @param <E> type de donnee demande
 */
public class AskRequest<E> {
	private E defaultValue;
	private E values[];
	private Askable description;
	private E choice;
	
	private PhysicalPlayer player;
	
	public AskRequest(Askable desc, E[] values, E defaultValue, PhysicalPlayer player) {
		this.defaultValue = defaultValue;
		this.values = values;
		this.description = desc;
		this.player = player;
		
		this.choice = null;
	}

	/**
	 * Renvoie la valeur par defaut
	 * @return valeur par defaut
	 */
	public E getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Renvoie les valeurs possibles
	 * @return valeurs pouvant etre choisies
	 */
	public E[] getValues() {
		return values;
	}

	/**
	 * Recupere une description de la valeur a choisir
	 * @return description de la valeur
	 */
	public Askable getDescription() {
		return description;
	}
	
	/**
	 * Definit le choix du joueur et notifie l'objet du joueur
	 * @param object objet choisi
	 */
	@SuppressWarnings("unchecked")
	public void setChoice(Object object) throws ClassCastException {
		this.choice = (E) object;
		
		// On notifie le joueur
		synchronized (player) {
			player.notify();
		}
	}

	public E getChoice() {
		return choice;
	}

	/**
	 * Renvoie un booleen indiquant si le joueur a fait son choix
	 * @return boolean indiquant si le choix est fait
	 */
	public boolean hasChosen() {
		return choice != null;
	}
}

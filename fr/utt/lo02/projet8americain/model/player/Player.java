package fr.utt.lo02.projet8americain.model.player;

import java.util.LinkedList;
import java.util.Observable;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;

/**
 * Classe abstraite representant un joueur.
 * 
 * Cette classe peut etre observee pour etre informe des changements dans la main.
 * 
 * @author Clement
 *
 */
public abstract class Player extends Observable {
	private String name;
	private boolean canPlay = false;
	private int points;
	private LinkedList<Card> hand;

	/**
	 * Indique si il s'agit vraiment du tour du joueur (il peut arriver qu'il
	 * puisse jouer hors de son tour)
	 */
	private boolean isTurn;

	public Player(String name) {
		this.name = name;
		this.hand = new LinkedList<Card>();
	}

	/**
	 * Joue la carte donnee
	 * @param card carte a jouer
	 * @return booleen indiquant si on en a fini pour ce tour
	 */
	public boolean play(Card card) {
		Player self;

		if (!this.canPlay()) { // tour deja termine
			return true;
		}
		
		// On évite la modification concurente
		synchronized(this.getHand()) {
			if (this.getHand().contains(card)) { // carte en main
				
				this.getHand().remove(card);
	
				self = this;
	
				// On lance l'action dans un autre thread en cas de demande blocante
				new Thread(new Runnable() {
					@Override
					public void run() {
						Game.getInstance().cardPlayed(self, card);
					}
				}).start();
	
				return true;
			}
		}
		return false;
	}

	/**
	 * Pioche une carte ou passe le tour selon la situation
	 */
	public void skip() {
		// Si c'est bien notre tour (un joueur peut avoir joue avant nous ou on peut ne pas etre oblige de jouer)
		if (this.canPlay() && this.isTurn()) {
			Player self = this;

			// On lance sur un thread separe (en cas de blocage car demande)
			new Thread(new Runnable() {
				@Override
				public void run() {
					Game.getInstance().pickCard(self);
				}
			}).start();
		}
	}

	/**
	 * Autorise ou non le joueur a jouer
	 * @param canPlay booleen indiquant si le joueur peut jouer
	 * @param isTurn indique si c'est vraiment le tour du joueur
	 */
	public void setCanPlay(boolean canPlay, boolean isTurn) {
		this.canPlay = canPlay;
		this.isTurn = isTurn;

		// On notifie les observers
		this.setChanged();
		this.notifyObservers();
	}

	/**
	 * Demande une valeur a un utilisateur
	 * @param description descriptif de la valeur (JOUEUR, COULEUR...)
	 * @param availableValues Valeurs possibles
	 * @param defaultValue Valeur par defaut
	 * @return valeur demandee
	 */
	public abstract <K> K ask(Askable description, K[] availableValues, K defaultValue);

	/**
	 * Renvoie si le joueur est encore en capacite de jouer
	 * @return booleen indiquant si le joueur peut encore jouer
	 */
	public boolean isPlaying() {
		return hand.size() > 0;
	}

	/**
	 * Change la main du joueur
	 * @param hand nouvelle main
	 */
	public void setHand(LinkedList<Card> hand) {
		this.hand = hand;
	}

	public String toString() {
		return this.name;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPoints() {
		return points;
	}

	public void setPoint(int points) {
		this.points = points;
	}

	public void addPoints(int points) {
		this.points += points;
	}

	public LinkedList<Card> getHand() {
		return hand;
	}

	public boolean isTurn() {
		return isTurn;
	}

	public boolean canPlay() {
		return canPlay;
	}
}

package fr.utt.lo02.projet8americain.model;

/**
 * Classe de gestion de l'ordre de jeu
 * @author Clement
 *
 */
public enum Direction {
	LEFT(-1), RIGHT(1);
	
	private int shift;
	
	private Direction(int shift) {
		this.shift = shift;
	}
	
	public int getShift() {
		return shift;
	}
	
	public Direction getOpposite() {
		return this == LEFT ? RIGHT : LEFT;
	}
	
	/**
	 * Modifie l'indice donne dans le sens donne par l'objet
	 * @param index Indice initial
	 * @return indice modifie
	 */
	public int apply(int index) {
		return index + shift;
	}
	
	/**
	 * Modifie l'indice donne selon le sens
	 * puis replace la valeur entre 0 et l'indice maximum donne.
	 * @param index indice initial
	 * @param maxIndex indice maximal exclus
	 * @return nouvel indice
	 */
	public int apply(int index, int maxIndex) {
		// On ajoute maxindex pour eviter les negatifs
		return (apply(index) + maxIndex) % maxIndex;
	}
}

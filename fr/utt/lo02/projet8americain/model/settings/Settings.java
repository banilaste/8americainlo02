package fr.utt.lo02.projet8americain.model.settings;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.function.Consumer;

import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.PlayHandlingModifier;
import fr.utt.lo02.projet8americain.model.carddealer.CardDealer;
import fr.utt.lo02.projet8americain.model.carddealer.RegularDealer;
import fr.utt.lo02.projet8americain.model.player.strategy.NormalStrategy;
import fr.utt.lo02.projet8americain.model.player.strategy.Strategy;
import fr.utt.lo02.projet8americain.model.playerunlockers.NextPlayerUnlocker;
import fr.utt.lo02.projet8americain.model.playerunlockers.PlayerUnlocker;
import fr.utt.lo02.projet8americain.model.pointcounter.PointCounter;
import fr.utt.lo02.projet8americain.model.pointcounter.PositiveCounter;
import fr.utt.lo02.projet8americain.model.variants.Variant;

/**
 * Classe contenant les differents parametres d'une partie
 * @author Clement
 *
 */
public abstract class Settings {
	private static boolean started = false;
	private static int turn;
	
	private static Variant variant = null;
    private static PlayerUnlocker unlocker = NextPlayerUnlocker.getInstance();
    private static PointCounter countingMethod = PositiveCounter.getVanillaInstance();
    private static ArrayList<PlayHandlingModifier> playModifiers = new ArrayList<PlayHandlingModifier> ();
    private static CardDealer cardDealer = RegularDealer.get();
    
	private static LinkedList<SettingsObserver> observers = new LinkedList<SettingsObserver>();
	private static Strategy strategy = NormalStrategy.get();
    
    /**
     * Renvoie vrai si partie peut commencer (tous les parametres sont definis)
     * @return booleen indiquant si la partie peut commencer
     */
    public static boolean canStart() {
    	return variant != null &&
    			unlocker != null &&
    			countingMethod != null &&
    			cardDealer != null &&
    			strategy != null;
    }
    
    /**
     * Renvoie le numero du tour actuel
     * @return numero du tour courant
     */
    public static int getTurn() {
    	return turn;
    }
    
    /**
     * Specifie que le tour est termine
     */
    public static void nextTurn() {
    	turn++;
    }
    
    /**
     * Defini une propriete numerique
     * @param property propriete
     * @param value valeur affectee
     */
    public static void set(Setting property, int value) {
    	if (property.getValue() != value) {
        	property.setValue(value);
	    	
	    	notifyObservers();
		}
    }
    
    /**
     * Defini une propriete booleene
     * @param property propriete
     * @param value valeur affectee
     */
    public static void set(Setting property, boolean value) {
    	if (property.getBoolean() != value) {
        	property.setValue(value);
	    	
	    	notifyObservers();
		}
    }

    /**
     * Initialise toutes les interfaces au debut de la partie
     */
	public static void init(Game game) {
		countingMethod.init(game.getPlayers());
		unlocker.init(game);
	}
    
    /*
     * Getters and setters
     */
	public static Variant getVariant() {
		return variant;
	}


	public static boolean isStarted() {
		return started;
	}


	public static void setStarted(boolean started) throws RuntimeException {
		// Si on n'est pas cense commencer
		if (!Settings.started && started && !canStart()) {
			throw new RuntimeException("unable to start the game, some parameters are not defined");
		}
		
		if (Settings.started != started) {
			Settings.started = started;
			
			notifyObservers();
		}
	}


	public static void setVariant(Variant variant) {
		if (!started && (Settings.variant == null || !Settings.variant.getFileName().equals(variant.getFileName()))) {
			Settings.variant = variant;
			notifyObservers();
		}
	}
	
	public static PlayerUnlocker getUnlocker() {
		return unlocker;
	}
	
	public static void setUnlocker(PlayerUnlocker unlocker) {
		if (!started && Settings.unlocker != unlocker) {
			Settings.unlocker = unlocker;
			notifyObservers();
		}
	}
	
	public static PointCounter getCountingMethod() {
		return countingMethod;
	}
	
	public static void setCountingMethod(PointCounter countingMethod) {
		if (!started && Settings.countingMethod != countingMethod) {
			Settings.countingMethod = countingMethod;
			notifyObservers();
		}
	}
	
	public static ArrayList<PlayHandlingModifier> getPlayModifiers() {
		return playModifiers;
	}
	
	public static CardDealer getCardDealer() {
		return cardDealer;
	}
	
	public static void setCardDealer(CardDealer cardDealer) {
		if (!started && Settings.cardDealer != cardDealer) {
			Settings.cardDealer = cardDealer;
			notifyObservers();
		}
	}
	
	public static void setStrategy(Strategy strategy) {
		if (!started && Settings.strategy != strategy) {
			Settings.strategy = strategy;
			notifyObservers();
		}
	}
	
	public static Strategy getStrategy() {
		return strategy;
	}

	/**
	 * Ajoute un observateur des parametres
	 * @param observer observateur
	 */
    public static void addObserver(SettingsObserver observer) {
    	synchronized (observers) {
    		observers.add(observer);
		}
    	
    	// Premiere update
    	observer.update();
    }

    /**
     * Supprime un observateur
     * @param observer observateurasupprimer
     */
	public static void removeObserver(SettingsObserver observer) {
		synchronized (observers) {
			observers.remove(observer);
		}
	}
	
    /**
     * Informe les observers que les parametres ont ete modifies
     */
    private static void notifyObservers() {
    	synchronized (observers) {
    		observers.forEach(new Consumer<SettingsObserver>() {
    			@Override
    			public void accept(SettingsObserver obs) {
    				obs.update();
    			}
    		});
		}
    	
    }
}

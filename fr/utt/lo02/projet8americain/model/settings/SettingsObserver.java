package fr.utt.lo02.projet8americain.model.settings;

/**
 * Observer special pour la classe Settings
 * 
 * @see Settings
 * @author Clement
 *
 */
public interface SettingsObserver {
	public void update();
}

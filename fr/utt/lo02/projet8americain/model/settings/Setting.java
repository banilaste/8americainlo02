package fr.utt.lo02.projet8americain.model.settings;

/**
 * Enumeration des differents parametres entier ou booleen de la partie.
 * 
 * Les valeurs doivent etre modifiees par la classe Settings pour etre observees
 * 
 * @see Settings
 * @author Clement
 *
 */
public enum Setting {
	PLAYERS_NUMBER(2),
	DECKS_NUMBER(1),
	VIRTUAL_PLAYER_DELAY(false),
	DISCARD_MIN_STORAGE(3),
	PICK_UNTIL_PLAY(false),
	CARD_NUMBER(5),
	NEGATIVE_MAX_POINTS(100),
	POSITIVE_COUNTING_GOAL(200),
	ERROR_PENALITY(false);

	private int value;

	private Setting(int defaultValue) {
		this.value = defaultValue;
	}

	private Setting(boolean defaultValue) {
		// On convertit un booleen en 0 ou 1
		this.value = defaultValue ? 1 : 0;
	}

	public int getValue() {
		return value;
	}

	public boolean getBoolean() {
		// Une valeur differente de 0 sera consideree comme vraie
		return value != 0;
	}

	/**
	 * Change la valeur du parametre.
	 * 
	 * La visibilite est package car les modifications doivent
	 * passer par la classe Settings observant les changements
	 * 
	 * @param value valeur entiere
	 */
	void setValue(int value) {
		this.value = value;
	}

	/**
	 * Change la valeur du parametre booleen.
	 * 
	 * La visibilite est package car les modifications doivent
	 * passer par la classe Settings observant les changements
	 * 
	 * @param value valeur booleene
	 */
	void setValue(boolean value) {
		this.value = value ? 1 : 0;
	}
}

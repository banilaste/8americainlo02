package fr.utt.lo02.projet8americain.model;

import java.util.ArrayDeque;
import java.util.LinkedList;

import fr.utt.lo02.projet8americain.model.settings.Setting;
import fr.utt.lo02.projet8americain.model.settings.Settings;

/**
 * Classe permettant de prendre un element aleatoire dans une liste de cartes
 * @author Clement
 *
 */
public class RandomizedArrayList extends LinkedList<Card> {
	private static final long serialVersionUID = 8642407279331401361L;

	/**
	 * Renvoie une carte piochee aleatoirement dans la liste
	 * @return carte piochee
	 * @throws IndexOutOfBoundsException exception levee quand la pioche n'a plus de cartes
	 */
	public Card pick() throws IndexOutOfBoundsException {
		if (this.size() == 0) {
			throw new IndexOutOfBoundsException("no more cards in stock pile");
		}

		// Indice de la valeur choisi aleatoirement
		int index = (int) Math.floor(Math.random() * this.size());
		
		// On renvoie l'element supprime
		return this.remove(index);
	}
	
	/**
     * Replace la defausse dans la pioche en laissant le nombre de
     * cartes suffisant pour jouer
     */
    public void restore(ArrayDeque<Card> discardPile) {
    	// Nombre de cartesagarder dans la defausse
    	int minStorage = Setting.DISCARD_MIN_STORAGE.getValue();
    	
    	ArrayDeque<Card> lastPlayed = new ArrayDeque<Card>();

    	// On garde la/les dernieres cartes jouees
    	for (int i = 0; i < minStorage; i++) {
			lastPlayed.add(discardPile.poll());
		}
    	
    	// Puis on vide tout dans la pioche
    	while(!discardPile.isEmpty()) {
    		this.add(discardPile.poll());
    	}
    	
    	// On remet ensuite les denieres cartes dans la defausse dans l'ordre precedant
    	for (int i = 0; i < minStorage; i++) {
    		discardPile.add(lastPlayed.poll());
		}
    }
	
	/**
	 * Genere les cartes d'un paquet avec un exemplaire de chaque
	 * carte
	 */
	public void generate() {
		this.generate(1);
	}
    
	/**
	 * Genere les cartes d'un paquet, avec le nombre specifie
	 * d'exemplaires de chaque carte.
	 * 
	 * Seul 2 jockers sont generes par paquet si la variante en a l'utilite
	 * @param duplications nombre d'exemplaires de chaque carte
	 */
	public void generate(int duplications) {
		// On vide tout
		this.clear();
		
		// On genere les cartes
		for (Value val : Value.values()) {
			for (Color col : Color.values()) {
				// Et cree n cartes de toutes les valeurs et couleurs possibles
				for (int i = 0; i < duplications; i++) {

					// On enleve deux couleurs pour le joker pour ne garder que deux cartes
					if (val != Value.JOKER) {
						try {
							this.add(new Card(col, val));
						} catch (InvalidCardException e) {
							// Normalement inpossible
							e.printStackTrace();
						}
					}
					
				}
			}
		}
		
		// Si la variante utilise le joker pour un effet
		if (Settings.getVariant().getEffects(Value.JOKER).length > 0) {
			// On ajoute des jokers rouge et noir
			for (int i = 0; i < duplications; i++) {
				try {
					this.add(new Card(Color.SPADE, Value.JOKER));
					this.add(new Card(Color.HEART, Value.JOKER));
				} catch (InvalidCardException e) {
					// normalement impossible
					e.printStackTrace();
				}
			}
		}
	}
}

package fr.utt.lo02.projet8americain.model.pointcounter;
import java.util.HashMap;
import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Value;
import fr.utt.lo02.projet8americain.model.player.Player;
import fr.utt.lo02.projet8americain.model.settings.Setting;

/**
 * Methode de comptage negatif
 * @author Clement
 *
 */
public class NegativeCounter implements PointCounter {
	/**
	 * Instance de comptage positif par defaut
	 */
	private static NegativeCounter instance;
	
	private boolean custom = true;
	private HashMap<Value, Integer> points;
	private int max;
	
	/**
	 * Constructeur vide du comptage negatif.
	 * 
	 * Une classe doit passer par getNextInstance() pour obtenir une instance de ce type.
	 */
	private NegativeCounter() {
		// Valeurs par defaut
		this(new HashMap<Value, Integer>());
		this.custom = false;
	}
	
	public NegativeCounter(HashMap<Value, Integer> points) {
		this.points = points;
		
		// On prends toutes les valeurs definies ou par defaut
		for (Value val : Value.values()) {
			// Si une valeur n'est pas definie
			if (!points.containsKey(val)) {
				// On prend celle par defaut
				points.put(val, val.getDefaultPoints());
			}
		}
	}
	
	@Override
	public void init(Player[] players) {
		// On prends la valeur parametree
		max = Setting.NEGATIVE_MAX_POINTS.getValue();
	}

	@Override
	public boolean countRound(Player[] players) {
		int max = 0;
		
		// On donne les pointsatous les joueurs
		for (Player player : players) {
			player.getHand().forEach((Card card) -> {
				// On donne les points associesachaque carte
				player.addPoints(points.get(card.getValue()));
			});
			
			if (player.getPoints() > max) {
				max = player.getPoints();
			}
		}
		
		// La partie est terminee si on depasse le max autorise
		return max >= this.max;
	}

	@Override
	public boolean countTurn(Player[] players, Player current) {
		// Si le joueur a une main vide, on termine la manche
		if (current.getHand().size() == 0) {
			return true;
		}
		
		return false;
	}

	@Override
	public Player getWinner(Player[] players) {
		// On prends le joueur ayant le minimum de points
		Player winner = players[0];
		
		for (int i = 1; i < players.length; i++) {
			if (players[i].getPoints() < winner.getPoints()) {
				winner = players[i];
			}
		}
		
		return winner;
	}	
	
	public String toString() {
		return "comptage negatif" + (custom ? " (customise)" : "");
	}
	
	/**
	 * Renvoie une methode de comptage instanciee pour la prochaine partie
	 * @return instance de comptage negatif
	 */
	public static NegativeCounter getVanillaInstance() {
		if (instance == null) {
			instance = new NegativeCounter();
		}
		
		return instance;
	}
}

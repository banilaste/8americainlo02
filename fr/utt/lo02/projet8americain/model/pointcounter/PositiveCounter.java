package fr.utt.lo02.projet8americain.model.pointcounter;
import fr.utt.lo02.projet8americain.model.player.Player;
import fr.utt.lo02.projet8americain.model.settings.Setting;

/**
 * Methode de comptage positive
 * 
 * @author Clement
 *
 */
public class PositiveCounter implements PointCounter {
	/**
	 * Instance du comptage par defaut
	 */
	private static PositiveCounter instance;
	
	// Variable definies par les settings
	private int points[] = null;
	private int goal = 0;
	private boolean custom = true;
	
	/**
	 * Nombre de joueurs ayant termine la partie
	 */
	private int playersOver = 0;
	
	/**
	 * Constructeur vide du comptage positif.
	 * 
	 * Une classe doit passer par getVanillaInstance() pour obtenir une autre instance.
	 */
	private PositiveCounter() {
		// Parametres par defaut
		this(new int[] {50, 20, 10});
		this.custom = false;
	}
	
	public PositiveCounter(int[] points) {
		this.points = points;
	}
	
	@Override
	public void init(Player[] players) {
		// Misea0 des points
		for (int i = 0; i< players.length; i++) {
			players[i].setPoint(0);
		}
		
		// On recupere les parametres definis ou ceux par defaut
		goal = Setting.POSITIVE_COUNTING_GOAL.getValue();
		
		playersOver = 0;
	}

	@Override
	public boolean countRound(Player[] players) {
		int max = 0;
		
		// On prends le plus grand nombre de points atteint
		for (int i = 0; i < players.length; i++) {
			if (players[i].getPoints() >= max) {
				max = players[i].getPoints();
			}
		}
		
		// On reinitialise les proprietes pour la prochaine manche
		playersOver = 0;
		
		return max >= goal;
	}

	@Override
	public boolean countTurn(Player[] players, Player current) {
		// Si le joueur courant a termine (on considere qu'il ne sera plus annonce apres)
		if (current.getHand().size() == 0) {
			// On lui ajoute les points selon sa place
			current.addPoints(points[playersOver]);
			playersOver ++;
			
			// Si il n'y a plus de pointsagagner ou qu'il ne reste qu'un joueur
			if (playersOver >= points.length) {
				// On termine la manche
				return true;
			}
			
			// S'il ne reste qu'un joueur
			if (playersOver >= players.length - 1) {
				// On le trouve
				for (Player player : players) {
					if (player.getHand().size() != 0) {
						// On lui donne les points restants
						player.addPoints(points[playersOver]);
						
						// Et on termine la manche
						return true;
					}
				}
			}
		}

		// Sinon on continue la manche
		return false;
	}

	@Override
	public Player getWinner(Player[] players) {
		int max = 0, index = -1;
		
		// On prends le plus grand nombre de points atteint
		for (int i = 0; i < players.length; i++) {
			if (players[i].getPoints() >= max) {
				max = players[i].getPoints();
				index = i;
			}
		}
		
		return players[index];
	}
	
	
	public String toString() {
		return "comptage negatif" + (custom ? " (customise)" : "");
	}
	
	/**
	 * Renvoie une methode de comptage instanciee pour la prochaine partie
	 * @return instance de comptage positif
	 */
	public static PositiveCounter getVanillaInstance() {
		if (instance == null) {
			instance = new PositiveCounter();
		}
		
		return instance;
	}
}

package fr.utt.lo02.projet8americain.model.pointcounter;
import fr.utt.lo02.projet8americain.model.player.Player;

/**
 * Interface contenant une methode de comptage des points.
 * Cette methode est appeleeala fin de chaque tour pour
 * savoir si la manche est terminee.
 * 
 * @author Clement
 *
 */
public interface PointCounter {
	/**
	 * Initialise les points des joueurs et les proprietes du comptage pour la partie
	 * @param players
	 */
    public void init(Player[] players);
    
    /**
     * Compte les pointsala fin d'une manche. Renvoie un booleen indiquant
     * si il est necessaire de faire une nouvelle manche. Initialise egalement
     * la prochaine manche.
     * @param players Joueurs de la partie
     * @return booleen indiquant si une nouvelle manche doit etre jouee
     */
    public boolean countRound(Player[] players);

    /**
     * Renvoie le gagnant selon la methode de comptage.
     * @param players Joueurs de la partie
     * @return Le gagnant si la partie est terminee ou null.
     */
    public Player getWinner(Player[] players);

    /**
     * Compte les points pour le tour courant. Renvoie true si la manche est terminee
     * @param players Joueurs de la partie
     * @param current Joueur venant de jouer
     * @return booleen indiquant si la manche est terminee
     */
	public boolean countTurn(Player[] players, Player current);

}

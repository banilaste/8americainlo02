package fr.utt.lo02.projet8americain.model;

import fr.utt.lo02.projet8americain.model.player.Player;
import fr.utt.lo02.projet8americain.model.settings.Settings;

/**
 * Classe abstraite representant un modificateur de jeu.
 * 
 * Ce modificateur s'applique avant les effets, et permet de decider
 * si la carte jouee donne lieuades effets supplementaire.
 * 
 * Il peut egalement annuler le tour en redonnant sa carte au joueur qui l'a posee
 * @author Clement
 *
 */
public abstract class PlayHandlingModifier {
	
	/**
	 * Enregistre le modificateur de jeu dans les parametres
	 */
	public final void enable() {
		if (!Settings.getPlayModifiers().contains(this)) {
			Settings.getPlayModifiers().add(this);
		}
	}
	
	/**
	 * Supprime le modificateur des parametres
	 */
	public final void disable() {
		Settings.getPlayModifiers().remove(this);
	}
	
	/**
	 * Applique le modificateur de jeu. Renvoie un boolean
	 * indiquant si le tour doit etre arrete ici.
	 * @param game partie courante
	 * @param source joueur ayant pose la carte
	 * @param card carte jouee ou null si le joueur pioche une carte
	 * @return booleen indiquant si le jeu doit etre annule
	 */
    public abstract boolean onCardPlayed(Game game, Player source, Card card);
}

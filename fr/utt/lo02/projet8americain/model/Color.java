package fr.utt.lo02.projet8americain.model;

/**
 * Enumeration contenant les couleurs existantes
 * @author Clement
 *
 */
public enum Color {
    DIAMOND("Carreau"),
    CLUB("Trefle"),
    HEART("Coeur"),
    SPADE("Pique");
	
	private String desc;
	
	private Color(String desc) {
		this.desc = desc;
	}
	
	public String toString() {
		return desc;
	}
	
	/**
	 * Renvoie la couleur associeeala chaine donnee,apartir de la description de la couleur
	 * @param str Description de la couleur
	 * @return couleur associee ou null
	 * @throws IllegalArgumentException erreur lorsque la valeur n'est pas trouvee
	 */
	public static Color fromString(String str) throws IllegalArgumentException {
		Color[] values = Color.values();
		for (int i = 0; i < values.length; i++) {
			if (values[i].toString().toUpperCase().equals(str.toUpperCase())) {
				return values[i];
			}
		}
		
		throw new IllegalArgumentException();
	}
}

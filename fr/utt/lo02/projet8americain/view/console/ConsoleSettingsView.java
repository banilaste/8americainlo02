package fr.utt.lo02.projet8americain.view.console;

import java.util.Arrays;

import fr.utt.lo02.projet8americain.controler.console.ConsoleGameControler;
import fr.utt.lo02.projet8americain.model.settings.Setting;
import fr.utt.lo02.projet8americain.model.settings.Settings;
import fr.utt.lo02.projet8americain.model.variants.Variant;

/**
 * "Vue" textuelle pour la configuration des parametres en mode console
 * @author Clement
 *
 */
public class ConsoleSettingsView implements Runnable {
	private ConsoleGameControler controler;

	private Thread thread;
	
	private boolean leave;
	
	public ConsoleSettingsView(ConsoleGameControler controler) {
		this.controler = controler;
		
		thread = new Thread(this);
		thread.start();
	}
	
	public void run() {
		int choice = -1;
		
		leave = false;
		
		do {
			System.out.println("= Parametres de la partie =");
			System.out.println("1) Variante : " + (Settings.getVariant() == null ? "non defini" : Settings.getVariant().getName()));
			System.out.println("2) Distribution : " + (Settings.getCardDealer() == null ? "non defini" : Settings.getCardDealer().toString()));
			System.out.println("3) Comptage des points : " + (Settings.getCountingMethod() == null ? "non defini" : Settings.getCountingMethod()));
			System.out.println("4) Nombre de joueurs : " + Setting.PLAYERS_NUMBER.getValue());
			System.out.println("5) Autres regles");
			
			if (Settings.canStart()) {
				System.out.println("6) Demarrer la partie !");
			}
			
			System.out.println("Que souhaitez vous faire ?");
		
			try {
				choice = ConsoleUtil.readInt();
				
				switch(choice) {
				case 1:
					chooseVariant();
					break;
				case 2:
					chooseDealingMethod();
					break;
				case 3:
					chooseCountingMethod();
					break;
				case 4:
					setIntegerParameter(Setting.PLAYERS_NUMBER, 2, 12);
					break;
					
				case 6:
					if (Settings.canStart()) {
						leave = true;
						controler.startGame();
						
						break;
					}
				default:
					ConsoleUtil.printException("action non definie");
				}
			} catch (InterruptedException e) {
				// On quitte le thread
				leave = true;
				
				thread.interrupt();
			}
		} while (leave == false);
	}
	
	/**
	 * Change un parametre entier dans les settings contraint entre deux bornes
	 * @param key nom de la propriete
	 * @param min valeur minimum
	 * @param max valeur maximum
	 * @throws InterruptedException 
	 */
	public void setIntegerParameter(Setting key, int min, int max) throws InterruptedException {
		System.out.println("Valeur pour le parametre " + key + " (entre " + min + " et " + max + ") :");
		
		int value = ConsoleUtil.readIntBetween(min, max);
		
		controler.setSetting(key, value);
	}

	/**
	 * Demande au joueur la methode de comptage utilisee
	 * @throws InterruptedException
	 */
	private void chooseCountingMethod() throws InterruptedException {
		int max = 2;
		
		System.out.println("Methodes de comptage de points disponibles :");
		System.out.println("1) Positif");
		System.out.println("2) Negatif");
		
		if (Settings.getVariant() != null && Settings.getVariant().hasCountingMethod()) {
			System.out.println("3) Variante");
			max = 3;
		}
		
		int choice = ConsoleUtil.readIntBetween(1, max);
		
		controler.setCountingMethod(choice);
	}

	/**
	 * Permet de choisir le nombre de carte distribuee
	 * @throws InterruptedException
	 */
	private void chooseDealingMethod() throws InterruptedException {
		int choice;
		
		System.out.println("Methode de distribution disponibles : ");
		System.out.println("1) Normale");
		System.out.println("2) Man et resta (choix du nombre de cartes)");
		System.out.println("Votre choix : ");
		
		choice = ConsoleUtil.readIntBetween(1, 2);
		
		switch(choice) {
		case 1:
			// Nombre de carte calcule en fonction des joueurs et des cartes dispos
			controler.setDealingMethod(ConsoleGameControler.NORMAL_DEALING);
			break;
		case 2:
			// Nombre de cartes choisit par le joueur
			controler.setDealingMethod(ConsoleGameControler.MAN_ET_RESTA_DEALING);
			
			System.out.println("Nombre de cartes a distribuer (entre 5 et 8) : ");
			choice = ConsoleUtil.readIntBetween(5, 8);
			
			controler.setSetting(Setting.CARD_NUMBER, choice);
			break;
		}
	}

	/**
	 * Permet de choisir la variante
	 * @throws InterruptedException 
	 */
	public void chooseVariant() throws InterruptedException {
		String[] variants = Variant.getFileNames();
		String choice = "";
		
		// On affiche d'abord les variantes dispos
		System.out.println("Variantes disponibles : " + String.join(", ", variants));
		
		System.out.println("Votre choix : ");
		
		do {
			choice = ConsoleUtil.readLine();			
		} while (!Arrays.asList(variants).contains(choice));
		
		// On change la variante via le controleur
		controler.setVariant(choice);
	}
	
	/**
	 * Quitte le parametrage
	 */
	public void leave() {
		leave = true;
		
		if (thread.isAlive()) {
			thread.interrupt();
		}
	}
}

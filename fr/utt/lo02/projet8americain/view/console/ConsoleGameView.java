package fr.utt.lo02.projet8americain.view.console;

import java.util.Observable;
import java.util.Observer;

import fr.utt.lo02.projet8americain.controler.console.ConsoleGameControler;
import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.GameActionListener;
import fr.utt.lo02.projet8americain.model.player.PhysicalPlayer;
import fr.utt.lo02.projet8americain.model.player.Player;
import fr.utt.lo02.projet8americain.model.settings.Settings;

/**
 * "Vue" textuelle du mode console pour la partie jeu.
 * Se contente d'envoyer les lignes non vides au controleur une fois activee
 * @author Clement
 *
 */
public class ConsoleGameView implements Observer, Runnable, GameActionListener {
	public final static String LEAVE = ".exit";

	/**
	 * Booleen indiquant si les observations ont ete initialisees
	 * Misatrue si la vue ecoute les utilisateurs (une fois la partie creee)
	 */
	private boolean initialized = false;
	private ConsoleGameControler controler;
	private Game game;
	private Thread thread;
	
	private int lastRound = -1;
	
	private ConsoleSettingsView settingsView;

	public ConsoleGameView(Game game, ConsoleGameControler controler, ConsoleSettingsView settingsView) {
		game.addObserver(this);
		game.addActionListener(this);

		this.controler = controler;
		this.game = game;
		this.settingsView = settingsView;
		
		thread = new Thread(this);
	}
	
	public void init() { // fonction necessaire car la partie doit etre cree
		// Si ce n'est pas deja fait
		if (!initialized) {
			initialized = true;

			// On observe tous les joueurs
			Player players[] = game.getPlayers();

			for (int i = 0; i < game.getPlayers().length; i++) {
				players[i].addObserver(this);
			}
			
			// On interrompt le thread des parametres
			settingsView.leave();
			
			// On demare le thread de lecture si ce n'est deja fait
			if (!thread.isAlive()) {
				thread.start();
			}
		}
	}

	public void run() {
		String input = null;

		boolean leave = false;
		System.out.println("Tapez " + LEAVE + " pour quitter.");

		do {
			try {
				input = ConsoleUtil.readLine();
			} catch (InterruptedException e) {
				System.err.println("Readline interrupted");
				leave = true;
				input = null;
			}

			if (input.equals(LEAVE)) {
				leave = true;
			} else if (input != null) {
				controler.onLine(input);
			}
		} while (leave == false);

		System.exit(0);
	}

	@Override
	public void update(Observable object, Object arg1) {
		if (object instanceof Game) {
			Game game = (Game) object;

			if (!initialized && Settings.isStarted()) {
				// On initialise une fois la partie commencee
				// car les joueurs sont crees lors du demarrage
				init();
			}

			if (game.getRound() != lastRound) {
				System.out.println("=== Manche " + game.getRound() + " ===");
				lastRound = game.getRound();
			}
		}

		else if (object instanceof PhysicalPlayer) {
			PhysicalPlayer player = ((PhysicalPlayer) object);

			if (player.isWaitingChoice()) { // si le joueur a un choixafaire
				Object[] values = player.getRequest().getValues();
				String possibilities[] = new String[values.length];

				// On convertit les possibilites sous formes de chaines
				for (int i = 0; i < values.length; i++) {
					possibilities[i] = values[i].toString();
				}

				// Puis on les affiche
				System.out.println("Veuillez choisir " + player.getRequest().getDescription());
				System.out.println("Choix possibles : " + String.join(",", possibilities));
			}

			// sinon s'il peut joueur
			else if (player.canPlay()) {
				System.out.println();
				System.out.println("Vous pouvez jouer.");
				System.out.println("Votre main : " + player.getHand());
				System.out.println("Derniere carte jouee : " + game.getDiscardPile().element());
			}

			// sinon (il ne peut pas jouer)
			else {
				System.out.println("Vous ne pouvez plus jouer.");
			}
		}
	}

	@Override
	public void onCardPlayed(Player source, Card card) {
		System.out.println(source.getName() + " a joue " + card);
	}

	@Override
	public void onCardsPicked(Player source, Card[] cards) {
		if (source == game.getPhysicalPlayer()) {
			System.out.print(source.getName() + " a pioche");

			for (Card c : cards) {
				System.out.print((c == cards[0] ? " " : ", ") + c);
			}
			System.out.println(".");
		} else {
			System.out.println(source.getName() + " a pioche " + cards.length + " carte(s)");
		}
	}
}

package fr.utt.lo02.projet8americain.view.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Classe de gestion de lecture des lignes en console.
 * 
 * Le singleton s'occupe de recuperer les lignes tandis que les methodes
 * statiques permettent de recuperer les lignes sans probleme de 
 * multi-threading.
 * 
 * @author Clement
 *
 */
public class ConsoleUtil implements Runnable {
	private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	private static ConcurrentLinkedQueue<String> buffer = new ConcurrentLinkedQueue<String>();
	
	@SuppressWarnings("unused")
	private static ConsoleUtil instance = new ConsoleUtil();
	
	private ConsoleUtil() {
		new Thread(this).start();
	}
	
	/**
	 * Recupere une ligne non vide depuis la console
	 * @return ligne lue
	 * @throws InterruptedException 
	 */
	public static synchronized String readLine() throws InterruptedException {
		String line;
		
		synchronized (buffer) {
			// On attend que la liste soit non vide
			while(buffer.isEmpty()) {
				buffer.wait();
			}
			
			// On recupere la ligne
			line = buffer.poll();
		}

		return line;
	}

	/**
	 * Lit un entier present sur une ligne
	 * @return entier lu
	 * @throws InterruptedException 
	 */
	public static synchronized int readInt() throws InterruptedException {
		int number = 0;
		boolean ok = false;
		
		synchronized (buffer) {
			do {
				// On attend que la liste soit non vide
				while(buffer.isEmpty()) {
					buffer.wait();
				}
				
				ok = true;
				
				// On recupere la ligne et on tente de la parser en entier
				try {
					number = Integer.parseInt(buffer.poll());
				} catch(NumberFormatException e) {
					ConsoleUtil.printException("invalid number");
					ok = false;
				}
			} while (!ok);
			
		}

		return number;
	}

	/**
	 * Lit un entier sur une ligne compris entre le min et max indique
	 * @param min valeur minimum incluse
	 * @param max valeur maximum incluse
	 * @return entier valide lu
	 * @throws InterruptedException 
	 */
	public static int readIntBetween(int min, int max) throws InterruptedException {
		int value = readInt();
		
		while (value < min || value > max) {
			printException("valeur invalide (doit etre entre " + min + " et " + max + " )");
			value = readInt();
		}
		
		return value;
	}

	
	public static void printException(Exception exception) {
		printException(exception.getMessage());
	}
	
	public static void printException(String intro, Exception exception) {
		printException(intro + " : " + exception.getMessage());
	}
	
	public static void printException(String message) {
		System.err.println(message);
	}
	
	@Override
	public void run() {
		String resultat = "";
		
		try {
			while (true) {
				resultat = "";
				
				// On lit une nouvelle ligne
				while (resultat.trim() == "") {
					resultat = br.readLine();
				}
				
				// On l'ajoute au buffer
				synchronized(buffer) {
					buffer.offer(resultat);

					buffer.notify();
				}
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}

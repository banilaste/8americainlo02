package fr.utt.lo02.projet8americain.view.gui;

import java.util.Observable;
import java.util.Observer;

import fr.utt.lo02.projet8americain.model.player.Player;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * Composant JavaFX gerant l'affichage des cartes d'un joueur virtuel
 * @author Clement
 *
 */
public class VirtualPlayerPane extends BorderPane implements Observer {
	private final static Font nameFont = new Font("Arial", 15);
	private final static Font cardsCountFont = Font.font("Arial", FontWeight.BOLD, 16);
	
	private Player player;

	private ImageView view;
	private Label label;
	
	private StackPane viewContainer;
	
	public VirtualPlayerPane(Player player) {
		this.player = player;
		
		viewContainer = new StackPane();
		
		// On met son nom
		Label nameLabel = new Label(player.getName());
		nameLabel.setFont(nameFont);
		nameLabel.setPadding(new Insets(5, 10, 0, 10));
		
		// L'image representant son jeu
		view = ImageLoader.getView("image/player_more_cards.png");
		label = new Label();
		label.setFont(cardsCountFont);
		label.setPadding(new Insets(0, 7, 3, 0));
		label.setTextFill(Color.WHITE);
		
		// On redimensionne proportionnellement
		view.setFitWidth(120);
		view.setFitHeight(120 * view.getImage().getHeight() / view.getImage().getWidth());

		viewContainer.getChildren().add(view);
		viewContainer.getChildren().add(label);
		
		StackPane.setAlignment(label, Pos.BOTTOM_RIGHT);
		
		// On place les composants
		this.setBottom(nameLabel);
		this.setCenter(viewContainer);
		
		// On met une petite marge
		this.setPadding(new Insets(10));

		// On observe le joueur
		player.addObserver(this);
	}


	@Override
	public void update(Observable arg0, Object arg1) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				label.setText("");
				
				// On change l'image selon le nombre de cartes dans la main
				switch (player.getHand().size()) {
				case 0:
					view.setImage(ImageLoader.get("image/player_0_cards.png"));
					break;
				case 1:
					view.setImage(ImageLoader.get("image/player_1_cards.png"));
					break;
				case 2:
					view.setImage(ImageLoader.get("image/player_2_cards.png"));
					break;
				case 3:
					view.setImage(ImageLoader.get("image/player_3_cards.png"));
					break;
				default:
					view.setImage(ImageLoader.get("image/player_more_cards.png"));
					label.setText(player.getHand().size() + "");
					break;
				}
			}
		});
		
	}
}

package fr.utt.lo02.projet8americain.view.gui.askdialog;

import java.util.Iterator;

import fr.utt.lo02.projet8americain.model.player.AskRequest;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.TextAlignment;
import javafx.util.Callback;

/**
 * Popup de demande de valeur. Les valeurs possibles sont affichees selon leur
 * methode <b>toString</b>
 * 
 * @author Clement
 *
 * @param <R> type de valeur attendue
 */
public class AskDialog<R> extends Dialog<R> {
	private ScrollPane container;
	private GridPane grid;
	private ButtonType validateButton = new ButtonType("Valider", ButtonData.OK_DONE);
	private R selected;
	
	public AskDialog(AskRequest<R> request) {
		grid = new GridPane();
		container = new ScrollPane();

		selected = request.getDefaultValue();
		
		// Parametrage du contenu
		container.setHbarPolicy(ScrollBarPolicy.NEVER);
		container.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		container.setStyle("-fx-background-color:transparent;");
		container.setPadding(new Insets(0));
		
		container.setContent(grid);
		
		addNodes(request.getValues());
		
		setHeaderText("Veuillez choisir " + request.getDescription().toString() + ".");
		
		getDialogPane().setContent(container);
		getDialogPane().getButtonTypes().add(validateButton);
		
		// On donne le callback
		setResultConverter(new Callback<ButtonType, R>() {
			@Override
			public R call(ButtonType button) {
				// Si on valide le choix
				if (button == validateButton) {
					return selected;
				}
				
				// Sinon on renvoie la valeur par defaut
				else {
					return request.getDefaultValue();
				}
			}
		});
	}

	/**
	 * Ajoute les element sur la grille
	 * @param values valeurs possibles
	 */
	protected void addNodes(R[] values) {
		Node node;
		Label label;
		
		// On prends la place pour 3 colonnes
		fitElements(Math.min(3, values.length), (values.length - 1) / 3 + 1);
		setSize(500, 300);
		
		for (int i = 0; i < values.length; i++) {
			// On affiche la description textuelle de l'objet
			label = new Label(values[i].toString());
			node = new BorderPane(label);
			
			label.setTextAlignment(TextAlignment.JUSTIFY);
			
			// On gere le clic dessus
			register(node, values[i]);
			
			// On ajouteaune grille de largeur 3
			getGrid().add(node, i % 3, i / 3);
		}
	}
	
	/**
	 * Ajoute la gestion du clic pour un element donne
	 * @param node noeud representant la valeur
	 * @param value valeur
	 */
	protected void register(Node node, R value) {
		node.setCursor(Cursor.HAND);
		
		// Gestion du clic
		node.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				selected = value;
				
				Iterator<Node> iter = grid.getChildren().iterator();
				
				// On enleve les styles des autres
				while (iter.hasNext()) {
					iter.next().setStyle(null);
					
				}
				
				node.setStyle("-fx-border-color: black; -fx-border-width: 1;");
			}
		});
		
		// Premiere selection
		if (value == selected) {
			node.setStyle("-fx-border-color: black; -fx-border-width: 1;");
		}
	}
	
	/**
	 * Change la taille de la zone de selection
	 * @param width largeur
	 * @param height hauteur
	 */
	protected void setSize(double width, double height) {
		grid.setPrefSize(width, height);
		container.setMaxSize(width, height);
	}
	
	/**
	 * Redimensionne les lignes et colonnes du tableau pour correspondre
	 * au nombre d'elements voulus
	 * @param columns nombre de colonnes
	 * @param rows nombre de lignes
	 */
	protected void fitElements(int columns, int rows) {
		ColumnConstraints column;
		RowConstraints row;
		
		// On definit la taille des elements de la grille
		for (int i = 0; i < columns; i++) {
			column = new ColumnConstraints();
			column.setHgrow(Priority.ALWAYS);
			getGrid().getColumnConstraints().add(column);
			
		}
		
		for (int i = 0; i < rows; i++) {
			row = new RowConstraints();
			row.setVgrow(Priority.ALWAYS);
			getGrid().getRowConstraints().add(row);
		}
	}
	
	/**
	 * Renvoie la grille contenant les elements
	 * @return grille
	 */
	protected GridPane getGrid() {
		return grid;
	}
}

package fr.utt.lo02.projet8americain.view.gui.askdialog;

import fr.utt.lo02.projet8americain.model.Color;
import fr.utt.lo02.projet8americain.model.player.AskRequest;
import fr.utt.lo02.projet8americain.view.gui.ImageLoader;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;

/**
 * Dialogue de demande de couleur
 * 
 * @author Clement
 *
 */
public class ColorAskDialog extends AskDialog<Color> {
	public ColorAskDialog(AskRequest<Color> request) {
		super(request);
	}

	@Override
	protected void addNodes(Color[] values) {
		Node node;
		
		// On prends la place pour deux lignes et deux colonnes
		fitElements(Math.min(2, values.length), (values.length - 1) / 2 + 1);
		setSize(300, 300);
		
		for (int i = 0; i < values.length; i++) {
			node = new BorderPane(ImageLoader.getView("image/cards/suit-" + values[i].name().toLowerCase() + ".png"));
			
			// On gere le clic dessus
			register(node, values[i]);
			
			// On ajouteala grille
			getGrid().add(node, i / 2, i % 2);
		}
	}
}

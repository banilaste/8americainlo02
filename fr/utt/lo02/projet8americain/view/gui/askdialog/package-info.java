/**
 * Package contenant les classe de gestion des fenetres de dialogue lors de la
 * demande d'une valeur a l'utilisateur
 * @author Clement
 *
 */
package fr.utt.lo02.projet8americain.view.gui.askdialog;
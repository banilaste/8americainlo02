package fr.utt.lo02.projet8americain.view.gui;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.settings.Setting;
import javafx.application.Platform;
import javafx.scene.layout.AnchorPane;

/**
 * Composant JavaFX gerant l'affichage des cartes jouees.
 * 
 * Misajour automatiquementachaque carte posee.
 * 
 * @author Clement
 *
 */
public class DiscardPilePane extends AnchorPane implements Observer {
	private CardView cardViews[] = new CardView[0];
	
	public DiscardPilePane() {
		Game.getInstance().addObserver(this);
		
		updateCards();
		
		AnchorPane.setRightAnchor(this, 0.0);
	}

	@Override
	public void update(Observable arg0, Object arg1) {		
		// On metajour les cartes dans le bon thread
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				updateCards();
			}
		});
	}
	
	public void organize() {
		this.getChildren().clear();
		
		// On affiche la derniere carte d'abord
		for (int i = cardViews.length - 1; i >= 0; i--) {
			// On ajoute l'element
			this.getChildren().add(cardViews[i]);
			
			// On lui change son ancreadroite (plus l'indice est eleve plus la carte est loin)
			AnchorPane.setRightAnchor(cardViews[i], i * 20.0);
		}
	}
	
	public void updateCards() {
		Game game = Game.getInstance();
		
		// On evite les modification concurentes
		synchronized (game) {
			int size = Math.min(Setting.DISCARD_MIN_STORAGE.getValue(), game.getDiscardPile().size());
			ArrayDeque<Card> pile = game.getDiscardPile();
			CardView cards[] = new CardView[size];
			Card next;
			
			Iterator<Card> iter = pile.iterator();
			
			for (int i = 0; i < cards.length && iter.hasNext(); i++) {
				// Pour chaque carte
				next = iter.next();
				
				// On regarde si une vue existe deja
				for (int j = 0; j < this.cardViews.length; j++) {
					if (this.cardViews[j] != null && this.cardViews[j].getCard().equals(next)) {
						cards[i] = this.cardViews[j];
						
						// On metanull pour eviter que deux fois la meme vue soit recuperee
						this.cardViews[j] = null;
					}
				}
				
				// Si on ne peut rien reutiliser
				if (cards[i] == null) {
					// On cree une nouvelle vue de carte non jouable
					cards[i] = new CardView(next);
				}
			}
			
			this.cardViews = cards;
		}
		
		// Et on reorganise le contenu
		organize();
	}
}

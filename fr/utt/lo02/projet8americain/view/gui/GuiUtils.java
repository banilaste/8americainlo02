package fr.utt.lo02.projet8americain.view.gui;

import fr.utt.lo02.projet8americain.controler.gui.IntegerValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * Classe contenant des fonctions utilesala vue console (affichage
 * des erreurs et generation de listes de valeurs)
 * @author Clement
 *
 */
public abstract class GuiUtils {
	/**
	 * Genere une liste de parametres entier avec une description
	 * @param from indice de debut compris
	 * @param to indice de fin compris
	 * @param label texteaajouterala description en plus du nombre
	 * @return liste des valeurs entieres
	 */
    public static ObservableList<IntegerValue> generateNumberedObservableList(int from, int to, String label) {
    	ObservableList<IntegerValue> strings = FXCollections.observableArrayList();
    	
    	for (int i = from; i <= to; i++) {
            strings.add(IntegerValue.createLabeled(i, label));
        }

    	return strings;
    }
    
    /**
     * Affiche une erreural'utilisateur
     * @param message message d'erreur
     */
    public static void showException(String message) {
    	showException(message, null);
    }

    /**
     * Affiche une erreural'utilisateur
     * @param exception exception en cause
     */
	public static void showException(Exception exception) {
		showException(exception.getMessage(), exception.getClass().toString());
	}
    
    /**
     * Affiche un erreural'utilisateur
     * @param message message d'erreur
     * @param title title de la boite affichee
     */
    public static void showException(String message, String title) {
    	Alert alert = new Alert(AlertType.ERROR);
    	alert.setTitle("Erreur");
    	alert.setHeaderText(title);
    	alert.setContentText(message);
    	
    	alert.showAndWait();
    }
}

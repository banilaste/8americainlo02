package fr.utt.lo02.projet8americain.view.gui;


import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.player.Player;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * Classe affichant une carte.
 * 
 * Cette carte peut etre jouee selon si elle est activee ou non
 * via enable/disablePlay().
 * 
 * Par default la carte n'est pas jouable.
 * @author Clement
 *
 */
public class CardView extends ScrollPane {
	private final static int HEIGHT = 150;
	
	private static ImageView image;
	
	private Card card;
	
	public CardView(Card card) {
		this.card = card;
		
		image = ImageLoader.getCardImageView(card);
		
		image.setFitHeight(HEIGHT);
		image.setFitWidth(HEIGHT * image.getImage().getWidth() / image.getImage().getHeight());
		
		this.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		this.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		
		this.setContent(image);
	
		this.setStyle("-fx-background-color:transparent;");
	}

	/**
	 * Renvoie la carte associe a la vue
	 * @return carte
	 */
	public Object getCard() {
		return card;
	}

	/**
	 * Rends cette carte jouable au clic
	 */
	public void enablePlay() {
		this.setCursor(Cursor.HAND);
		
		// On ecoute les clics dessus
		this.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Player player = Game.getInstance().getPhysicalPlayer();
				
				// Si on peut jouer
				if (player.canPlay()) {
					// On joue la carte
					Game.getInstance().getPhysicalPlayer().play(card);
				}
			}
		});
	}

	/**
	 * Desactive la jouabilite de la carte au clic
	 */
	public void disablePlay() {
		this.setCursor(Cursor.DEFAULT);

		this.setOnMouseClicked(null);
		
	}
}

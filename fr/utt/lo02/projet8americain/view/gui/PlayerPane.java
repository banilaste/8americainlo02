package fr.utt.lo02.projet8americain.view.gui;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;
import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Color;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.player.AskRequest;
import fr.utt.lo02.projet8americain.model.player.Askable;
import fr.utt.lo02.projet8americain.model.player.PhysicalPlayer;
import fr.utt.lo02.projet8americain.model.settings.Settings;
import fr.utt.lo02.projet8americain.view.gui.askdialog.AskDialog;
import fr.utt.lo02.projet8americain.view.gui.askdialog.ColorAskDialog;
import javafx.application.Platform;
import javafx.scene.layout.HBox;

/**
 * Composant JavaFX gerant l'affichage des cartes du joueur reel.
 * @author Clement
 *
 */
public class PlayerPane extends HBox implements Observer {
	private PhysicalPlayer player;
	private HashMap<Card, CardView> cards = new HashMap<Card, CardView>();
	private boolean dialogShowed = false;
	
	public PlayerPane(PhysicalPlayer player) {
		this.player = player;
		
		player.addObserver(this);
		
		// Premiere miseajour des cartes
		updateCards();
	}

	@Override
	public void update(Observable o, Object arg) {
		
		// On accede au thread de l'interface
		Platform.runLater(new Runnable() {
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				// On metajour les cartes
				updateCards();
				
				// Si une demande est faiteal'utilisateur
				if (player.isWaitingChoice() && !dialogShowed) {
					dialogShowed = true;
					
					AskRequest<?> request = player.getRequest();
					AskDialog<?> dialog;
					
					// Si on demande une couleur
					if (request.getDescription() == Askable.COLOR) {
						// On met le panneau specifiqueala situation
						dialog = new ColorAskDialog((AskRequest<Color>) request);
					} else {
						// Sinon on met le generique
						dialog = new AskDialog<>(request);
					}

					try {
						// On renvoie la valeur recuperee
						request.setChoice(dialog.showAndWait().get());
						
					} catch(ClassCastException e) {
						GuiUtils.showException("Les types des valeurs entrees sont incompatibles.", "Erreur lors de la recuperation du choix");
					}
					
					dialogShowed = false;
				}
			}
		});
	}
	
	/**
	 * Metajour les cartes
	 */
	private void updateCards() {
		LinkedList<Card> hand = player.getHand();
		HashMap<Card, CardView> newCards = new HashMap<Card, CardView>();
		Game game = Game.getInstance();
		
		synchronized(hand) {
			Iterator<Card> iter = hand.iterator();
			Card next;
			Card lastPlayed = game.getDiscardPile().element();
	
			// On reprends les anciennes cartes
			while(iter.hasNext()) {
				next = iter.next();
				
				// On prends soit la vue precedente, soit une nouvelle
				if (cards.containsKey(next)) {
					newCards.put(next, cards.get(next));
				} else {
					newCards.put(next, new CardView(next));
				}
				
				// On active ou desactive la jouabilite des cartes selon la
				// derniere carte jouee
				if (player.canPlay() && !player.isWaitingChoice() && Settings.getVariant().canPlayOver(next, lastPlayed)) {
					newCards.get(next).enablePlay();
				} else {
					newCards.get(next).disablePlay();
				}
			}
		}
		
		// On les met dans l'interface
		this.getChildren().clear();
		this.getChildren().addAll(newCards.values());
	}
}

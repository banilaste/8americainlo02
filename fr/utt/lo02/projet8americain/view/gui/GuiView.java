package fr.utt.lo02.projet8americain.view.gui;

import java.io.IOException;

import fr.utt.lo02.projet8americain.model.settings.Settings;
import fr.utt.lo02.projet8americain.model.settings.SettingsObserver;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Classe gerant les differentes vues de l'interface graphique
 * @author Clement
 *
 */
public class GuiView extends Application implements SettingsObserver {
	private Stage stage;
	
	public GuiView() { }

	@Override
	public void start(Stage stage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("fxml/settings.fxml"));
		
		this.stage = stage;
		
		// On observe la partie pour attendre qu'elle demarre
		Settings.addObserver(this);
		
		// Fermeture de la fenetre
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				// Arret du programme
				Platform.exit();
				System.exit(0);
			}
		});
		
		stage.setTitle("8 Americain - Configuration");
		stage.setScene(new Scene(root));
		stage.setResizable(false);
		stage.show();
	}

	@Override
	public void update() {
		synchronized(this) {
			if (Settings.isStarted()) {
				// Plus besoin d'ecouter les changements
				Settings.removeObserver(this);
				
				// On demarre l'interface de la partie sur le thread qui va bien
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						startGame();
						
					}
				});
			}
		}
	}

	/**
	 * Demarre la partie en affichant la fenetre de jeu
	 */
	private void startGame() {
		// On charge la fenetre de jeu
		try {
			Parent root = FXMLLoader.load(getClass().getResource("fxml/game.fxml"));
			
			stage.setScene(new Scene(root, 800, 600));
			stage.setTitle("8 Americain");
			stage.show();
		} catch (IOException e) {
			GuiUtils.showException(e);
			e.printStackTrace();
		}
	}
}

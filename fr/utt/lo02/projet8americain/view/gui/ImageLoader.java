package fr.utt.lo02.projet8americain.view.gui;

import java.io.IOException;
import java.util.HashMap;
import fr.utt.lo02.projet8americain.model.Card;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Classe statique gerant la recuperation des images.
 * @author Clement
 *
 */
public abstract class ImageLoader {
	private static HashMap<String, Image> imagesLoaded = new HashMap<String, Image>();

	/**
	 * Renvoie une image
	 * @param path chemin de l'image
	 * @return image chargee ou reprise du cache
	 * @throws IOException
	 */
	public static Image get(String path) {
		if (imagesLoaded.containsKey(path)) {
			return imagesLoaded.get(path);
		} else {
			Image image;

			try {
				image = new Image(ImageLoader.class.getResourceAsStream(path));
			} catch(NullPointerException e) {
				GuiUtils.showException(path, "Image non trouvee");

				// Sjnon on renvoie une image vide
				return null;
			}

			imagesLoaded.put(path, image);

			return image;
		}
	}
	
	/**
	 * Renvoie l'image demandee encapsulee dans une ImageView
	 * @param path chemin de l'image
	 * @return view sur l'image demandee
	 */
	public static ImageView getView(String path) {
		return new ImageView(get(path));
	}
	
	/**
	 * Renvoie une image de la carte donnee. Une image vide est renvoyee le cas echeant
	 * @param card carte
	 * @return vue sur une image de la carte
	 */
	public static ImageView getCardImageView(Card card) {
		String fileName = card.getValue().name().toLowerCase() + "_" + card.getColor().name().toLowerCase();

		if (fileName.charAt(0) == 'c') {
			fileName = fileName.substring(1);
		}
		
		// On tente de renvoyer l'image
		return new ImageView(get("image/cards/" + fileName + ".png"));

	}
}
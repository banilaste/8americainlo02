package fr.utt.lo02.projet8americain.controler;

import fr.utt.lo02.projet8americain.controler.console.ConsoleGameControler;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.view.console.ConsoleGameView;
import fr.utt.lo02.projet8americain.view.console.ConsoleSettingsView;
import fr.utt.lo02.projet8americain.view.gui.GuiView;

public class Main {
	
	public static void main(String args[]) {
		Game game = Game.getInstance();
		
		// Demarage de la vue console
		ConsoleGameControler controller = new ConsoleGameControler(game);

		ConsoleSettingsView settings = new ConsoleSettingsView(controller);
		new ConsoleGameView(game, controller, settings);
	

		// Puis de la vue graphique
		GuiView.launch(GuiView.class, args);
	}
}

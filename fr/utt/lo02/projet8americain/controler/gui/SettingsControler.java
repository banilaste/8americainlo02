package fr.utt.lo02.projet8americain.controler.gui;

import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.carddealer.ManEtRestaDealer;
import fr.utt.lo02.projet8americain.model.carddealer.RegularDealer;
import fr.utt.lo02.projet8americain.model.player.strategy.EasyStrategy;
import fr.utt.lo02.projet8americain.model.player.strategy.HardStrategy;
import fr.utt.lo02.projet8americain.model.player.strategy.NormalStrategy;
import fr.utt.lo02.projet8americain.model.player.strategy.Strategy;
import fr.utt.lo02.projet8americain.model.pointcounter.NegativeCounter;
import fr.utt.lo02.projet8americain.model.pointcounter.PointCounter;
import fr.utt.lo02.projet8americain.model.pointcounter.PositiveCounter;
import fr.utt.lo02.projet8americain.model.settings.Setting;
import fr.utt.lo02.projet8americain.model.settings.Settings;
import fr.utt.lo02.projet8americain.model.settings.SettingsObserver;
import fr.utt.lo02.projet8americain.model.variants.InvalidVariantFileException;
import fr.utt.lo02.projet8americain.model.variants.Variant;
import fr.utt.lo02.projet8americain.view.gui.GuiUtils;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import java.io.IOException;

/**
 * Controleur pour la vue graphique gerant la fenetre de parametrage
 * @author Clement
 *
 */
public class SettingsControler implements SettingsObserver {

	// Composants JavaFX
	@FXML
	private ChoiceBox<IntegerValue> nbPlayerChoiceBox;
	@FXML
	private ChoiceBox<IntegerValue> nbDeckChoiceBox;
	@FXML
	private ChoiceBox<ObjectValue<Strategy>> difficultyChoiceBox;
	@FXML
	private ChoiceBox<ObjectValue<PointCounter>> countingMethodChoiceBox;
	@FXML
	private ChoiceBox<StringValue> variantsChoiceBox;
	@FXML
	private RadioButton manEtRestaRadioBtn;
	@FXML
	private ChoiceBox<IntegerValue> nbCardsChoiceBox;
	@FXML
	private Button startGameBtn;

	/**
	 * Collection contenant les methodes de comptage disponibles, la methode de la variante peut s'ajouter selon le choix
	 */
	@SuppressWarnings("unchecked")
	private ObservableList<ObjectValue<PointCounter>> countingMethodChoices = FXCollections.observableArrayList(
			new ObjectValue<PointCounter>(PositiveCounter.getVanillaInstance(), "Comptage positif"),
			new ObjectValue<PointCounter>(NegativeCounter.getVanillaInstance(), "Comptage negatif")
			);
	
	private ObjectValue<PointCounter> variantCounting;

	@SuppressWarnings("unchecked")
	@FXML
	private void initialize() {
		// Contenus numeriques generes
		nbPlayerChoiceBox.setItems(GuiUtils.generateNumberedObservableList(2, 10, "joueurs"));
		nbDeckChoiceBox.setItems(GuiUtils.generateNumberedObservableList(1, 2, "decks"));
		nbCardsChoiceBox.setItems(GuiUtils.generateNumberedObservableList(5, 8, ""));

		// Contenus "statiques"
		difficultyChoiceBox.setItems(FXCollections.observableArrayList(
				new ObjectValue<Strategy>(EasyStrategy.get(), "Facile"),
				new ObjectValue<Strategy>(NormalStrategy.get(), "Normal"),
				new ObjectValue<Strategy>(HardStrategy.get(), "Difficile")
		));
		variantsChoiceBox.setItems(FXCollections.observableArrayList(getVariantStringValues(Variant.getFileNames())));
		countingMethodChoiceBox.setItems(countingMethodChoices);

		// On associe le choix du nombre de carte avec 
		nbCardsChoiceBox.disableProperty().bind(Bindings.not(manEtRestaRadioBtn.selectedProperty()));

		/*
		 * On observe les changements
		 */

		// Boutons radio
		manEtRestaRadioBtn.getToggleGroup().selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			@Override
			public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
				setDealingMethod(newValue);
			}
		});

		// Methode de comptage
		countingMethodChoiceBox.valueProperty().addListener(new ChangeListener<ObjectValue<PointCounter>>() {
			@Override
			public void changed(ObservableValue<? extends ObjectValue<PointCounter>> observable, ObjectValue<PointCounter> oldValue, ObjectValue<PointCounter> newValue) {
				Settings.setCountingMethod(newValue.getValue());
			}
		});

		// Variante
		variantsChoiceBox.valueProperty().addListener(new ChangeListener<StringValue>() {
			@Override
			public void changed(ObservableValue<? extends StringValue> observable, StringValue oldValue, StringValue newValue) {
				if (newValue != null) {
					setVariant(newValue.getValue());
				}
			}
		});

		difficultyChoiceBox.valueProperty().addListener(new ChangeListener<ObjectValue<Strategy>>() {
			@Override
			public void changed(ObservableValue<? extends ObjectValue<Strategy>> observable,
					ObjectValue<Strategy> oldValue, ObjectValue<Strategy> newValue) {
				Settings.setStrategy(newValue.getValue());
			}
		});
		
		startGameBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Game.getInstance().start();
			}
		});

		listenSetting(nbDeckChoiceBox, Setting.DECKS_NUMBER);
		listenSetting(nbPlayerChoiceBox, Setting.PLAYERS_NUMBER);
		listenSetting(nbCardsChoiceBox, Setting.CARD_NUMBER);

		// On active les delais pour les bots
		Settings.set(Setting.VIRTUAL_PLAYER_DELAY, true);
		
		// On observe les parametres (mettra les valeurs correctement dans les bon endroits)
		Settings.addObserver(this);
	}

	/**
	 * Renvoie les noms de variantes reels associesaleur nom de fichier
	 * @param fileNames noms de fichiers
	 * @return noms de variantes associesaleur nom de fichier
	 */
	private StringValue[] getVariantStringValues(String[] fileNames) {
		StringValue values[] = new StringValue[fileNames.length];

		try {
			for (int i = 0; i < fileNames.length; i++) {
				values[i] = new StringValue(fileNames[i], Variant.getName(fileNames[i]));
			}
		} catch (IOException e) {
			/*
			 * Normalement si les fichiers sont definis correctement cette erreur ne peut pas se produire
			 */
			GuiUtils.showException(e);
		}

		return values;
	}

	/**
	 * Change la methode de distribution selon le bouton selectionne
	 * @param selectedToggle bouton selectionne
	 */
	public void setDealingMethod(Toggle selectedToggle) {
		if (selectedToggle != manEtRestaRadioBtn) {
			Settings.setCardDealer(RegularDealer.get());
		} else {
			Settings.setCardDealer(ManEtRestaDealer.get());
		}
	}

	/**
	 * Defini la variante selon le nom choisi
	 * @param choice nom de variante
	 */
	public void setVariant(String choice) {
		try {
			Variant var = Variant.get(choice);
			Settings.setVariant(var);

			// On prend par defaut le comptage selon la variante
			if (var.hasCountingMethod()) {
				if (variantCounting != null) {
					countingMethodChoices.remove(variantCounting);
				}
				
				variantCounting = new ObjectValue<PointCounter>(var.getCountingMethod(), "Variante");
				
				countingMethodChoices.add(variantCounting);
				countingMethodChoiceBox.setValue(variantCounting);

			} else if (countingMethodChoices.contains(variantCounting)) {
				countingMethodChoiceBox.setValue(countingMethodChoiceBox.getItems().get(0));
				countingMethodChoices.remove(variantCounting);
				variantCounting = null;
			}

			return;
		} catch (IOException | InvalidVariantFileException exception) {
			if (exception instanceof IOException) {
				GuiUtils.showException(exception);
			} else {
				GuiUtils.showException(exception.getMessage(), "Erreur lors du chargement de la variante");

			}

			// On retire la variante de la liste
			variantsChoiceBox.getItems().remove(new StringValue(choice));
			variantsChoiceBox.setValue(null);
		}


	}

	/**
	 * Observe les changement d'une liste et change un parametre entier des parametres
	 * @param choiceBox liste deroulante contenant les valeursachoisir
	 * @param property prioprieteachanger
	 */
	public void listenSetting(ChoiceBox<IntegerValue> choiceBox, Setting property) {
		choiceBox.valueProperty().addListener(new ChangeListener<IntegerValue>() {

			@Override
			public void changed(ObservableValue<? extends IntegerValue> o, IntegerValue l, IntegerValue newValue) {
				// On change directement la valeur dans les parametres
				Settings.set(property, newValue.getValue());
			}

		});
	}

	@Override
	public void update() {
		// On lance les modifications dans le bon thread
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				updateData();
			}
		});
	}

	/**
	 * Metajour les donnees affichees selon les parametres
	 */
	private void updateData() {
		// Si la partie a commence, on n'a plus besoin de mettreajour
		if (Settings.isStarted()) {
			return;
		}

		nbPlayerChoiceBox.setValue(new IntegerValue(Setting.PLAYERS_NUMBER.getValue()));
		nbDeckChoiceBox.setValue(new IntegerValue(Setting.DECKS_NUMBER.getValue()));
		nbCardsChoiceBox.setValue(new IntegerValue(Setting.CARD_NUMBER.getValue()));

		if (Settings.getVariant() != null) {
			variantsChoiceBox.setValue(new StringValue(Settings.getVariant().getFileName()));
		} else {
			variantsChoiceBox.setValue(null);
		}


		countingMethodChoiceBox.setValue(new ObjectValue<PointCounter>(Settings.getCountingMethod()));
		difficultyChoiceBox.setValue(new ObjectValue<Strategy>(Settings.getStrategy()));
	}
}
package fr.utt.lo02.projet8americain.controler.gui;

/**
 * Objet associant une chaineaune description.
 * 
 * Est equivalentaObjectValue<String> mais permet l'instanciation de tableaux
 * (impossible de faire des tableaux de type generiques)
 * @author Clement
 *
 */
public class StringValue extends ObjectValue<String> {
	public StringValue(String value) {
		super(value);
	}
	public StringValue(String value, String label) {
		super(value, label);
	}

}

package fr.utt.lo02.projet8americain.controler.gui;

import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.player.Player;
import fr.utt.lo02.projet8americain.model.player.VirtualPlayer;
import fr.utt.lo02.projet8americain.view.gui.DiscardPilePane;
import fr.utt.lo02.projet8americain.view.gui.PlayerPane;
import fr.utt.lo02.projet8americain.view.gui.VirtualPlayerPane;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

/**
 * Controleur de la fenetre de jeu pour la vue graphique
 * @author Clement
 *
 */
public class GameControler {

	@FXML
	private HBox playersPane;
	
	
	@FXML
	private ImageView stockPileImage;
	
	@FXML
	private BorderPane rootPane;
	
	public GameControler() {}

	@FXML
	private void initialize() {
		Game game = Game.getInstance();
		
		// On cree les panneaux pour les joueurs virtuels
		Player[] players = game.getPlayers();
		VirtualPlayerPane pane;

		for (int i = 0; i < players.length; i++) {
			if (players[i] instanceof VirtualPlayer) {
				pane = new VirtualPlayerPane(players[i]);

				playersPane.getChildren().add(pane);
			}
		}
		
		// Cartes du joueur reel
		rootPane.setBottom(new PlayerPane(Game.getInstance().getPhysicalPlayer()));

		// Defausse
		rootPane.setCenter(new DiscardPilePane());
		
		// On place l'action de pioche sur la pioche
		stockPileImage.setCursor(Cursor.HAND);
		stockPileImage.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				// On pioche
				game.getPhysicalPlayer().skip();
			}
		});
	}
}

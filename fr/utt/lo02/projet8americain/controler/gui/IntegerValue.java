package fr.utt.lo02.projet8americain.controler.gui;

/**
 * Object contenant une valeur entier et ayant une description
 * @author Clement
 *
 */
public class IntegerValue {
	private int value;
	private String label;
	
	public IntegerValue(int value) {
		this.label = Integer.toString(value);
		this.value = value;
	}
	
	public IntegerValue(int value, String label) {
		this.label = label;
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	public boolean equals(Object val) {
		if (val instanceof IntegerValue) {
			return ((IntegerValue) val).getValue() == getValue();
		}
		
		return false;
	}
	
	public String toString() {
		return label;
	}
	
	/**
	 * Cree une valeur dont le texte sera la valeur suivie du texte
	 * @param value valeur
	 * @param text texte suivant la valeur a afficher
	 * @return valeur entiere associeasa description
	 */
	public static IntegerValue createLabeled(int value, String text) {
		StringBuffer buf = new StringBuffer();
		buf.append(value);
		buf.append(" ");
		buf.append(text);
		
		return new IntegerValue(value, buf.toString());
	}
}

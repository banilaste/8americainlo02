package fr.utt.lo02.projet8americain.controler.gui;

/**
 * Objet associant un objetasa description
 * @author Clement
 *
 * @param <E> classe sauvegardee
 */
public class ObjectValue<E> {
	private E value;
	private String label;
	
	public ObjectValue(E value, String label) {
		this.label = label;
		this.value = value;
	}
	
	public ObjectValue(E value) {
		this(value, "");
	}

	public E getValue() {
		return value;
	}
	
	@SuppressWarnings("rawtypes")
	public boolean equals(Object val) {
		if (val instanceof ObjectValue) {
			return ((ObjectValue) val).getValue() == getValue();
		}
		
		return false;
	}
	
	public String toString() {
		return label;
	}
}

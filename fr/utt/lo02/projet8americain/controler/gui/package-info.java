/**
 * Package contenant les classes du controleur de l'interface
 * graphique du jeu.
 * @author Clement
 *
 */
package fr.utt.lo02.projet8americain.controler.gui;
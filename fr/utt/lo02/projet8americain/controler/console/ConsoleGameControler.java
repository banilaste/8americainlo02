package fr.utt.lo02.projet8americain.controler.console;

import java.io.IOException;

import fr.utt.lo02.projet8americain.model.Card;
import fr.utt.lo02.projet8americain.model.Game;
import fr.utt.lo02.projet8americain.model.carddealer.ManEtRestaDealer;
import fr.utt.lo02.projet8americain.model.carddealer.RegularDealer;
import fr.utt.lo02.projet8americain.model.player.PhysicalPlayer;
import fr.utt.lo02.projet8americain.model.pointcounter.NegativeCounter;
import fr.utt.lo02.projet8americain.model.pointcounter.PositiveCounter;
import fr.utt.lo02.projet8americain.model.settings.Setting;
import fr.utt.lo02.projet8americain.model.settings.Settings;
import fr.utt.lo02.projet8americain.model.variants.InvalidVariantFileException;
import fr.utt.lo02.projet8americain.model.variants.Variant;
import fr.utt.lo02.projet8americain.view.console.ConsoleUtil;
import fr.utt.lo02.projet8americain.view.gui.GuiUtils;

/**
 * Controleur de jeu, gere les actions de l'utilisateur physique en fonction des entrees clavier
 * @author Clement
 *
 */
public class ConsoleGameControler {
	public final static String PICK = ".pioche";
	
	// Methode de distribution
	public final static int NORMAL_DEALING = 0;
	public final static int MAN_ET_RESTA_DEALING = 1;
	
	// Methode de comptage
	public final static int POSITIVE_COUNTING = 1;
	public final static int NEGATIVE_COUNTING = 2;
	public final static int VARIANT_COUNTING = 3;
	
	private Game game;
	
	public ConsoleGameControler(Game game) {
		this.game = game;
	}
	
	/**
	 * Lorsque l'utilisateur entre une ligne
	 * @param line ligne entree
	 */
	public void onLine(String line) {
		PhysicalPlayer player = game.getPhysicalPlayer();
		
		try {
			// Si on attend un choix de l'utilisateur
			if (player.isWaitingChoice()) {
				Object[] values = player.getRequest().getValues();
				
				// On prend les possibilites sous formes de chaines
				for (int i = 0; i < values.length; i++) {
					
					// Et on regarde si une des chaine correspond
					if (values[i].toString().equals(line)) {
						
						try {
							// Et on informe du choix si c'est le cas
							player.getRequest().setChoice(values[i]);
						} catch(ClassCastException e) {
							GuiUtils.showException("Les types des valeurs entrees sont incompatibles.", "Erreur lors de la recuperation du choix");
						}
					}
				}
	
				
			}
			
			// Sinon si le joueur peut jouer
			else if (player.canPlay()) {
				// Si il pioche
				if (line.equals(PICK)) {
					player.skip();
					
				} else {
					Card card = Card.parseCard(line);
					
					player.play(card);
				}
			}
			
		} catch(Exception e) {
			// affichage des erreurs non captures
			System.err.println("erreur: " + e.getMessage());
		}
	}

	/**
	 * Change un parametre dans les Settings
	 * @param key parametreamodifier
	 * @param value nouvelle valeur
	 */
	public void setSetting(Setting key, int value) {
		Settings.set(key, value);
	}
	
	/**
	 * Change la methode de distribution selon la methode indiquee
	 * @param dealingMethod methode de comptage representee par un entier
	 */
	public void setDealingMethod(int dealingMethod) {
		switch (dealingMethod) {
		case NORMAL_DEALING:
			Settings.setCardDealer(RegularDealer.get());
			break;
		case MAN_ET_RESTA_DEALING:
			Settings.setCardDealer(ManEtRestaDealer.get());
			break;
		}
	}
	
	/**
	 * Defini la variante selon son nom
	 * @param choice nom de la variante choisie
	 */
	public void setVariant(String choice) {
		try {
			Variant var = Variant.get(choice);
			Settings.setVariant(var);
			
			// On prend par defaut le comptage selon la variante
			if (var.hasCountingMethod()) {
				Settings.setCountingMethod(var.getCountingMethod());
			}
		} catch (IOException exception) {
			ConsoleUtil.printException(exception);
			
		} catch (InvalidVariantFileException exception) {
			ConsoleUtil.printException("erreur lors du chargement de la variante", exception);
		}
		
	}

	/**
	 * Change la methode de comptage selon la methode indiquee
	 * @param method methode de comptage representee par un entier
	 */
	public void setCountingMethod(int method) {
		switch(method) {
		case POSITIVE_COUNTING:
			Settings.setCountingMethod(PositiveCounter.getVanillaInstance());
			break;
			
		case NEGATIVE_COUNTING:
			Settings.setCountingMethod(NegativeCounter.getVanillaInstance());
			break;
			
		case VARIANT_COUNTING:
			try {
				Settings.setCountingMethod(Settings.getVariant().getCountingMethod());
			} catch(NullPointerException e) {
				ConsoleUtil.printException("variant is not defined");
			}
			break;
		}
	}

	/**
	 * Demarre la partie
	 */
	public void startGame() {
		game.start();
	}
}
